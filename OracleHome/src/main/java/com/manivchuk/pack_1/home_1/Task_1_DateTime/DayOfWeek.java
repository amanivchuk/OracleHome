package com.manivchuk.pack_1.home_1.Task_1_DateTime;

public enum DayOfWeek {
    SUNDAY(1), MONDAY(2), TUESDAY(3), WEDNESDAY(4), THURSDAY(5), FRIDAY(6), SATURDAY(7);

    int index;

    DayOfWeek(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public static DayOfWeek valueOf(int index){
        DayOfWeek[] days = DayOfWeek.values();
        DayOfWeek day = days[index-1];
        return day;
    }
}
