package com.manivchuk.pack_1.home_2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
    public static String reverseString(String str){
        StringBuilder sb = new StringBuilder(str);
        String result = sb.reverse().toString();
        System.out.println(result);
        return result;
    }

    public static boolean isPalindrome(String str){
        str = str.toLowerCase().replace(" ","");
        int strLength = str.length();
        for(int i = 0; i < strLength/2; i++)
            if (str.charAt(i) != str.charAt(strLength - i - 1))
                return false;
        return true;
    }

    public static String notMoreTen(String str){
        if(str.length() > 10){
            String result = (String)str.subSequence(0,6);
            System.out.println(result);
            return result;
        }else{
            StringBuilder sb = new StringBuilder(str);
            for(int i = str.length(); i < 12;i++){
                sb.append("o");
            }
            System.out.println("else = " +sb.toString());
            return sb.toString();
        }
    }

    public static String changeFirstAndLastWord(String str){
        StringBuilder result = new StringBuilder();
        String[] mas = str.split(" ");
        ArrayList<String> listWord = new ArrayList<>(Arrays.asList(mas));
        listWord.add(listWord.size(), listWord.get(0));
        listWord.set(0, listWord.get(listWord.size() - 2));
        listWord.remove(listWord.size() - 2);
        for(String word : listWord){
            System.out.println(word);
            result.append(word).append(" ");
        }
        System.out.println(result.toString());
        return result.toString();
    }
    public static boolean hasABC(String str){
        boolean result = false;
        char a = 'a';
        char b = 'b';
        char c = 'c';
        char[] st = str.toCharArray();
        for(int i = 0; i < st.length;i++){
            if((st[i] == a) || (st[i] == b) || (st[i] == c))
                result = true;
        }
        return result;
    }
    public static boolean strFormatDate(String str){
        Pattern pattern = Pattern.compile("^\\d{2}\\.\\d{2}\\.\\d{4}$");
        boolean date = false;
        Matcher matcher = pattern.matcher(str);
        if(matcher.find()){
            date = true;
        }
        return date;
    }
    public static boolean strPostAddress(String str){
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
        "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher m = pattern.matcher(str);
        boolean result = false;
        if(m.find()){
            result = true;
        }
        return result;
    }
    public static String findCorrectTel(String str){
        Pattern phoneCheck = Pattern.compile("\\+[0-9]{1}\\([0-9]{3}\\)[0-9]{3}-[0-9]{2}-[0-9]{2}");
        Matcher mat = phoneCheck.matcher(str);
        String phone;
        StringBuilder count = new StringBuilder();

        while(mat.find()){
            count.append(mat.group() + "; ");
        }
        phone = count.toString();

        return phone;
    }

    public static String Zamena(String s) {
        String[] slova = s.split("\\ ");
        String zamena = new String();

        zamena = zamena.concat(slova[slova.length-1]).concat(" ");

        for(int i=1; i<slova.length-1 ;i++){

            zamena = zamena.concat(slova[i]).concat(" ");
        }
        zamena = zamena.concat(slova[0]);
        return zamena;

    }

    public static String ZamenaPred(String s) {
        String[] sentence = s.concat(" ").split("\\. ");
        String zamena = new String();
        StringBuilder s1 = new StringBuilder();

        for(int i=0; i<sentence.length; i++){

            s1.append(Zamena(sentence[i]) + ". ");
        }
        zamena = s1.toString();

        return zamena;
    }
}
