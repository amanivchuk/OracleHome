package com.manivchuk.pack_1.home_2.Task_2_Markdown;

import com.github.rjeschke.txtmark.*;

import java.io.File;
import java.io.IOException;

public class ParserMarkdown {
    public static void main(String[] args) throws IOException {
        ParserMarkdown pm = new ParserMarkdown();
        pm.view();
    }

    public void view(){
        String result = null;
        try {
            result = Processor.process(new File("files/index.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("<html>\n" + "<body>");
        System.out.print(result);
        System.out.println("</body>\n" + "</html>");
    }
}
