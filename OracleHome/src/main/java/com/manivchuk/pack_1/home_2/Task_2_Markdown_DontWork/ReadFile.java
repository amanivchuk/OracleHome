package com.manivchuk.pack_1.home_2.Task_2_Markdown;

import java.io.*;

public class ReadFile {
    public static String readFile(String file){
        StringBuilder sb = new StringBuilder();
        String str;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            while((str = reader.readLine())!= null){
                sb.append(str);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
