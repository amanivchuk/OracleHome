package com.manivchuk.pack_1.home_3;

import java.util.Arrays;

public class GenericStorage<T> {
    private Object[] array;
    private int size;

    public GenericStorage() {
        array = new Object[10];
    }

    public GenericStorage(int size) {
        array = new Object[size];
    }

    public void add(T arg){
        if(size < array.length){
            array[size++] = arg;
            System.out.println("Element added!");
        }else
            System.out.println("Array is full!");
    }
    public T get(int index){
        T result = null;
        if(index >= 0 && index < size){
            result = (T)array[index];
        } else{
            System.out.println("Index isn't correct!");
        }
        return result;
    }
    public T[] getAll(){
        return (T[])array;
    }
    public void update(int index, T obj){
        if(index >= 0 && index < size){
            if(array[index] != null){
                array[index] = obj;
            } else{
                System.out.println("Object isn't update!");
            }
        }else {
            System.out.println("Bad index!");
        }
    }
    public void delete(int index){
        if(index >=0 && index < size){
            int arg = array.length - index - 1;
            array[index] = null;
            System.arraycopy(array, index+1, array, index, arg);
            System.out.println("Elements deleted!");
            size--;
        }else {
            System.out.println("Bad index!");
        }
    }
    public void delete(T obj){
        for(int i = 0; i < array.length; i++){
            if(obj.equals(array[i])){
                delete(i);
            }
        }
    }
    public int sizeArray(){
        return size;
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }
}
