package com.manivchuk.pack_1.home_3;

import java.util.Arrays;

public class SomeGeneric {
    public static void main(String[] args) {
        GenericStorage<Integer> gs = new GenericStorage<>();
        System.out.println("Now in array: " + gs);
        System.out.println("Size of array = " + gs.sizeArray());

        for(int i = 1;i <= 5; i++)
            gs.add(i);
        System.out.println("Elements of array: " + gs + "\n and size = " + gs.sizeArray());

        System.out.println("1st index = " + gs.get(0));
        System.out.println("2nd index = " + gs.get(1));
        System.out.println("5th index = " + gs.get(4));

        System.out.println("Looking all array : " + Arrays.toString(gs.getAll()));

        System.out.println("Replace element");
        gs.update(2,25);
        System.out.println("Elements of array: " + gs + "\n and size = " + gs.sizeArray());

        System.out.println("Delete element on index");
        gs.delete(2);
        System.out.println("Elements of array: " + gs + "\n and size = " + gs.sizeArray());

        System.out.println("Delete element");
        gs.delete(new Integer(4));
        System.out.println("Elements of array: " + gs + "\n and size = " + gs.sizeArray());

    }
}
