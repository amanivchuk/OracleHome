package com.manivchuk.pack_1.home_4.task_4_1;

public class Comp  implements Comparable<Comp>{
    private String name;
    private int price;

    public Comp(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int compareTo(Comp o) {
        int result;
        result = name.compareTo(o.getName());
        if(result != 0) return result;
        result = Integer.compare(getPrice(), o.getPrice());
        return result;
    }

    @Override
    public String toString() {
        return "Comp{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
