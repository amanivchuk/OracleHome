package com.manivchuk.pack_1.home_4.task_4_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class StartProg {
    public static void main(String[] args) {
        ArrayList<Integer> integerList = new ArrayList<>(Arrays.asList(1, 3, 2, 6, 4, 5));
        Collections.sort(integerList);
        System.out.println(integerList);
        System.out.println("===============================================");

        ArrayList<String> stringList = new ArrayList<>(Arrays.asList("a", "w", "f", "q", "t", "h", "b"));
        Collections.sort(stringList);
        System.out.println(stringList);
        System.out.println("===============================================");

        ArrayList<Car> carList = new ArrayList<>(Arrays.asList(new Car("Ggg", 21), new Car("Aaa", 12), new Car("Qfd", 442), new Car("Bbb", 32)));
        //Collections.sort(carList); //Compile Error!
        System.out.println();

        ArrayList<Comp> compList = new ArrayList<>(Arrays.asList(new Comp("Lenovo", 231), new Comp("Asus", 122), new Comp("Acer", 432), new Comp("Dell", 760), new Comp("Asus", 120)));
        Collections.sort(compList);
        System.out.println(compList);
    }
}
