package com.manivchuk.pack_1.home_4.task_4_2.part_1;

public interface MyDeque<E> {
    void addFirst(E e); // добавить элемент в начало списка
    void addLast(E e); // добавить элемент в конец списка
    E removeFirst(); // получить элемент из начала списка и удалить его
    E removeLast(); // получить элемент из конца списка и удалить его
    E getHead(); // получить элемент из начала списка, не удаляя его
    E getLast(); // получить элемент из конца списка, не удаляя его
    boolean contains(Object o); //  проверить, содержится ли объект o в списке (с помощью equals)
    void clear(); // очистить список
    Object[] toArray(); //  вернуть массив элементов из списка (сохраняя упорядоченность элементов списка)
    int size(); //  вернуть количество элементов в списке
    boolean containsAll(MyDeque<? extends E> deque); // проверить, содержит ли список все элементы списка deque
}
