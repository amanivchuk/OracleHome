package com.manivchuk.pack_1.home_4.task_4_2.part_1;

public class StartProg {
    public static void main(String[] args) {
        MyDequeImpl<Integer> myDeque = new MyDequeImpl<>();
        for(int i= 0; i < 3; i++){
            myDeque.addFirst(new Integer(i));
        }
        System.out.println("size = " + myDeque.size());
        System.out.println("getLast = " + myDeque.getLast());
        System.out.println("getHead = " + myDeque.getHead());

        System.out.println("=======================");
        Object[] a =  myDeque.toArray();
        System.out.println("size array = " + a.length);
        for(int i = 0; i < a.length;i++){
            System.out.print(a[i] + ", ");
        }
        System.out.println();

        System.out.println("=======================");
        myDeque.addFirst(12);
        System.out.println("getHead = " + myDeque.getHead());
        myDeque.addLast(32);
        System.out.println("getLast = " + myDeque.getLast());
        myDeque.removeFirst();
        myDeque.removeLast();
        System.out.println("getHead = " + myDeque.getHead());
        System.out.println("getLast = " + myDeque.getLast());

        myDeque.addFirst(45);
        System.out.println("Contains 45 = " + myDeque.contains(45));

        myDeque.clear();
        myDeque.getHead();
        myDeque.getLast();
    }
}
