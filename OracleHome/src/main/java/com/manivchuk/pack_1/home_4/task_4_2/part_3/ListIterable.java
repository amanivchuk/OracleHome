package com.manivchuk.pack_1.home_4.task_4_2.part_3;

public interface ListIterable<E> {
    ListIterator<E> listIterator();
}
