package com.manivchuk.pack_1.home_4.task_4_2.part_3;

import java.util.Iterator;

public interface ListIterator<E> extends Iterator<E> {
    boolean hasPrevious();  // проверяет, есть ли предыдущий элемент для выборки методом previous
    E previous();   // возвращает предыдущий элемент
    void set(E e);  // заменяет элемент, который на предыдущем шаге был возвращен next/previous на данный элемент
    void remove(); // удаляет элемент, который на предыдущем шаге был возвращен next/previous

}
