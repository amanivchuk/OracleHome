package com.manivchuk.pack_1.home_4.task_4_2.part_3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyDequeImpl<E> implements MyDeque<E> {
    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl();
    }

    @Override
    public ListIterator listIterator() {
        return new ListIteratorImpl();
    }


    private class Node{
        E element;
        Node next;
        Node prev;

        public Node(E element, Node next, Node prev) {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }

        @Override
        public String toString() {
            return "element=" + element;
        }
    }
    private Node head;
    private Node tail;
    private int size;

    public MyDequeImpl() {
        this.size = 0;
    }

    @Override
    public void addFirst(E e) {
        Node tmp = new Node(e, head, null);
        if(head != null) {head.prev = tmp;}
        head = tmp;
        if(tail == null) {tail = tmp;}
        size++;
        System.out.println("adding: " + e);
    }

    @Override
    public void addLast(E e) {
        Node tmp = new Node(e, null, tail);
        if(tail != null) {tail.next = tmp;}
        tail = tmp;
        if(head == null) {head = tmp;}
        size++;
        System.out.println("adding: " + e);
    }

    @Override
    public E removeFirst() {
        if(size == 0) throw new NoSuchElementException();
        Node tmp = head;
        head = head.next;
        head.prev = null;
        size--;
        System.out.println("deleted: " + tmp.element);
        return tmp.element;
    }

    @Override
    public E removeLast() {
        if(size == 0) throw new NoSuchElementException();
        Node tmp = tail;
        tail = tail.prev;
        tail.next = null;
        System.out.println("deleted: " + tmp.element);
        size--;
        return tmp.element;
    }

    @Override
    public E getHead() {
        if(size != 0)
            return head.element;
        else
             return null;
    }

    @Override
    public E getLast() {
        if(size != 0)
            return tail.element;
        else
            return null;
    }

    @Override
    public boolean contains(Object o) {
       Node tmp = head;
        while(tmp != null){
            if(tmp.element.equals(o)){
                return true;
            }
            tmp = tmp.next;
        }
        return false;
    }

    @Override
    public void clear() {
        while (size > 0){
            Node tmp = head.next;
            head = tmp;
            size--;
        }
        head = null;
        tail = null;
        System.out.println("Cleared!!!");
    }

    @Override
    public Object[] toArray() {
        Object[] objects = new Object[size];
        int i = 0;
        for(Node n = head; n != null; n = n.next){
            objects[i++] = n.element;
        }
        return objects;
    }

    @Override
    public int size(){ return size; }

    @Override
    public boolean containsAll(MyDeque<? extends E> deque) {
        ArrayList<E> list1 = new ArrayList<>();
        ArrayList<E> list2 = new ArrayList<>(Arrays.<E>asList((E[]) deque.toArray()));
        for(Node n = head; n != null; n = n.next){
            list1.add(n.element);
        }
        return list1.containsAll(list2);
    }


    private class IteratorImpl implements Iterator<E> {
        Node current = tail;
        private boolean flag = false;
        @Override
        public boolean hasNext() {
            return (current != null);
        }

        @Override
        public E next() {
            E value = current.element;
            current = current.prev;
            flag = true;
            return value;
        }

        @Override
        public void remove() {
            if(flag) {
                //current.element = null;
                throw new UnsupportedOperationException("remove() not available!");
            }
            else throw new IllegalStateException();
        }
    }

    private class ListIteratorImpl implements ListIterator {
        Node currentPrev = head;
        Node currentNext = tail;
        private boolean flag = false;
        @Override
        public boolean hasPrevious() {
            return currentPrev != null;
        }

        @Override
        public Object previous() {
            E value = currentPrev.element;
            currentPrev = currentPrev.next;
            flag = true;
            return value;
        }

        @Override
        public void set(Object o) {

        }

        @Override
        public boolean hasNext() {
            return (currentNext != null);
        }

        @Override
        public E next() {
            E value = currentNext.element;
            currentNext = currentNext.prev;
            flag = true;
            return value;
        }

        @Override
        public void remove() {
            if(flag) {
                //current.element = null;
                throw new UnsupportedOperationException("remove() not available!");
            }
            else throw new IllegalStateException();
        }
    }
}
