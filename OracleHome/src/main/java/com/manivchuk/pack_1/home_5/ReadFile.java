package com.manivchuk.pack_1.home_5;

import com.manivchuk.pack_1.home_5.exception.DictionaryException;
import com.manivchuk.pack_1.home_5.operation.ConsoleHelper;
import com.manivchuk.pack_1.home_5.operation.Operation;

import java.io.*;

public class ReadFile {
    private static StringBuilder sb = new StringBuilder();

    public static StringBuilder getSb() throws DictionaryException {
        if (sb.length() == 0){
            throw new DictionaryException("Error find dictionary!!!");
        }
        return sb;
    }

    public static void readFile(String operation){
        String dirPath = "src/main/java/com/manivchuk/pack_1/home_5/files";
        File f = new File(dirPath);
        File[] files = f.listFiles();
        for(File file : files){
            if((operation.equalsIgnoreCase(file.getName())))
                loadDictionary(file.getAbsoluteFile());
        }
    }

    private static void loadDictionary(File file) {
        String str;
        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            while((str = br.readLine())!= null){
                sb.append(str).append("\n");
            }
        } catch (IOException e) {
            System.out.println("Error!");
        }
    }
}
