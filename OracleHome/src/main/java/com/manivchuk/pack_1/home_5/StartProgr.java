package com.manivchuk.pack_1.home_5;

import com.manivchuk.pack_1.home_5.command.CommandExecutor;
import com.manivchuk.pack_1.home_5.operation.ConsoleHelper;
import com.manivchuk.pack_1.home_5.operation.Operation;

public class StartProgr {
    public static void main(String[] args) {
//        Translator t = new Translator();
//        t.addToMapSource();

        Operation operation;
        do{
            operation = ConsoleHelper.askOperation();
            try {
                //ReadFile.readFile(operation);
                CommandExecutor.execute(operation);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }while (operation != Operation.EXIT);
    }
}
