package com.manivchuk.pack_1.home_5.command;

public interface Command {
    void execute() throws InterruptedException;
}
