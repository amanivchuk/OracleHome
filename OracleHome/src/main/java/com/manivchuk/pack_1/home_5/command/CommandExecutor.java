package com.manivchuk.pack_1.home_5.command;

import com.manivchuk.pack_1.home_5.operation.Operation;

import java.util.HashMap;
import java.util.Map;

public class CommandExecutor {
    private static Map<Operation,Command>  map;
    static {
        map = new HashMap<>();
        map.put(Operation.EN_RU, new EnRuCommand());
        map.put(Operation.RU_EN, new RuEnCommand());
        map.put(Operation.TranslateFILE, new TranslateFileCommand());
        map.put(Operation.EXIT, new ExitCommand());
    }

    public static final void execute(Operation operation) throws InterruptedException {
        map.get(operation).execute();
    }
}
