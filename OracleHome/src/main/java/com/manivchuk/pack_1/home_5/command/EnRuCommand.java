package com.manivchuk.pack_1.home_5.command;

import com.manivchuk.pack_1.home_5.translate.Translate;

public class EnRuCommand implements Command {
    @Override
    public void execute() throws InterruptedException {
        Translate translate = new Translate();
        translate.translateWord();
    }
}
