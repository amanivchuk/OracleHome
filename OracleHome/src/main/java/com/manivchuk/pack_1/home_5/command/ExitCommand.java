package com.manivchuk.pack_1.home_5.command;


import com.manivchuk.pack_1.home_5.operation.ConsoleHelper;

public class ExitCommand implements Command {
    @Override
    public void execute() throws InterruptedException {
        ConsoleHelper.writeMessage("");
    }
}
