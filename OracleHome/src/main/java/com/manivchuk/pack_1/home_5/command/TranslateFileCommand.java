package com.manivchuk.pack_1.home_5.command;

import com.manivchuk.pack_1.home_5.exception.DictionaryException;
import com.manivchuk.pack_1.home_5.operation.ConsoleHelper;
import com.manivchuk.pack_1.home_5.translate.Translate;

import java.io.*;

public class TranslateFileCommand implements Command {
    @Override
    public void execute() throws InterruptedException {
        ConsoleHelper.writeMessage("Enter path to file which translate:");
        ConsoleHelper.writeMessage("src/main/java/com/manivchuk/pack_1/home_5/files/enFile  OR");
        ConsoleHelper.writeMessage("src/main/java/com/manivchuk/pack_1/home_5/files/ruFile");
        String pathToFile = ConsoleHelper.readMessage();
        String strForTranslate = loadFileWhichTranslate(pathToFile);
        Translate translate = new Translate();
        try {
            System.out.println(translate.translateFile(strForTranslate));
        } catch (DictionaryException e) {
            e.printStackTrace();
        }


    }
    public String loadFileWhichTranslate(String path){
        File file = new File(path);
        StringBuilder forTranslate = new StringBuilder();
        String str;
        char[] buf = new char[1024];
        int count = 0;
        try(InputStreamReader isr = new InputStreamReader(new FileInputStream(file))) {
            while((count = isr.read(buf)) != -1){
                forTranslate.append(new String(buf,0,count));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Укажите правильный путь к файлу!");
        } catch (IOException e) {
            e.printStackTrace();
        }

       /* try (BufferedReader readFile = new BufferedReader(new InputStreamReader(new FileInputStream(file)))){
            while ((str = readFile.readLine()) != null){
                forTranslate.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        System.out.println(forTranslate.toString());
        return forTranslate.toString();
    }
}
