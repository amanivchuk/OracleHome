package com.manivchuk.pack_1.home_5.exception;

public class DictionaryException extends Exception{
    public DictionaryException() {
    }

    public DictionaryException(String message) {
        super(message);
    }
}
