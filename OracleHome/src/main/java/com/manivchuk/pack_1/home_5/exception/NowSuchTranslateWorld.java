package com.manivchuk.pack_1.home_5.exception;

public class NowSuchTranslateWorld extends Exception {
    public NowSuchTranslateWorld() {
    }

    public NowSuchTranslateWorld(String message) {
        super(message);
    }
}
