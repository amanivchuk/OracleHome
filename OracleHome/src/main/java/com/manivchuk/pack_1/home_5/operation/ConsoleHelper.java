package com.manivchuk.pack_1.home_5.operation;

import com.manivchuk.pack_1.home_5.ReadFile;
import com.manivchuk.pack_1.home_5.exception.DictionaryException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleHelper {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage(String str){
        System.out.println(str);
    }

    public static String readMessage() {
        try {
            String input = reader.readLine();
            if(input.equalsIgnoreCase(String.valueOf(Operation.EXIT)))
                throw new InterruptedException();
            return input;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Operation askOperation(){
        writeMessage("Choose translate option:");
        for (Operation o : Operation.values()){
            writeMessage(o.getId() + " : " + o.toString());
        }
        Operation operation;
        while (true){
            int choosenId = Integer.parseInt(readMessage());
            try {
                operation = Operation.getAllowedOperationByOrdinal(choosenId);
                chosenDictionaryForLoadonMap(operation);
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (DictionaryException e) {
                e.printStackTrace();
            }
        }
        return operation;
    }

    public static void chosenDictionaryForLoadonMap(Operation operation) throws DictionaryException {
        switch (operation){
            case EN_RU: ReadFile.readFile("en-ru");
                break;
            case RU_EN: ReadFile.readFile("ru-en");
                break;
        }
    }

}
