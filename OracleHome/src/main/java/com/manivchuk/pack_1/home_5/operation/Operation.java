package com.manivchuk.pack_1.home_5.operation;

public enum Operation {
    EN_RU(0), RU_EN(1), TranslateFILE(2), EXIT(3);

    private int id;
    Operation(int id) {this.id = id;}
    public int getId() {return id;}

    public static Operation getAllowedOperationByOrdinal(Integer id) throws InterruptedException {
        switch (id){
            case 0: return Operation.EN_RU;
            case 1: return Operation.RU_EN;
            case 2: return Operation.TranslateFILE;
            case 3: return Operation.EXIT;
            default: throw new InterruptedException();
        }
    }
}
