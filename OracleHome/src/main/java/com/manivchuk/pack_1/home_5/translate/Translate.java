package com.manivchuk.pack_1.home_5.translate;

import com.manivchuk.pack_1.home_5.exception.DictionaryException;
import com.manivchuk.pack_1.home_5.exception.NowSuchTranslateWorld;
import com.manivchuk.pack_1.home_5.operation.ConsoleHelper;
import com.manivchuk.pack_1.home_5.operation.Operation;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Translate {
    public void translateWord() {

        Map<String, String> mapEnRu = Translator.mapDictionary();
        ConsoleHelper.writeMessage("Enter world for translate!");
        while (true) {
            String userWorld = ConsoleHelper.readMessage();
            if (userWorld.equals("q")) {
                break;
            }
            try {
                String translateWorld = translateWorld(userWorld, mapEnRu);
                ConsoleHelper.writeMessage(userWorld + " -> " + translateWorld);

            } catch (NowSuchTranslateWorld nowSuchTranslateWorld) {
                System.out.println("Incorrect world for translateWord!");
                nowSuchTranslateWorld.printStackTrace();
            }
            ConsoleHelper.writeMessage("Enter new world or enter \"q\" ");
        }
    }
    private String translateWorld(String userWorld, Map<String, String> mapEnRu) throws NowSuchTranslateWorld {
        for(Map.Entry e : mapEnRu.entrySet()){
            if(userWorld.equalsIgnoreCase((String) e.getKey())){
                return (String) e.getValue();
            }
        }
        return userWorld;
    }
    public String translateFile(String translateStr) throws DictionaryException {
        StringBuilder result = new StringBuilder();
        Pattern pattern =  Pattern.compile(
                "[" +                   //начало списка допустимых символов
                        "а-яА-ЯёЁ" +    //буквы русского алфавита
                        "\\d" +         //цифры
                        "\\s" +         //знаки-разделители (пробел, табуляция и т.д.)
                        "\\p{Punct}" +  //знаки пунктуации
                        "]" +                   //конец списка допустимых символов
                        "*");
        Matcher matcher = pattern.matcher(translateStr);
        boolean flagLanguage = matcher.matches();

        if(!flagLanguage){
            ConsoleHelper.chosenDictionaryForLoadonMap(Operation.EN_RU);
        }if(flagLanguage){
            ConsoleHelper.chosenDictionaryForLoadonMap(Operation.RU_EN);
        }
        Map<String, String> mapDictionary = Translator.mapDictionary();
        //String[] forTranslate = translateStr.split("\\s+\\s*");
        String[] forTranslate = (translateStr + " ").split("\\p{P}?[ \\t\\n\\r]+");
        for(String str : forTranslate){
            try {
                result.append(translateWorld(str, mapDictionary)).append(" ");
            } catch (NowSuchTranslateWorld nowSuchTranslateWorld) {
                nowSuchTranslateWorld.printStackTrace();
            }
        }
        return result.toString();
    }

}
