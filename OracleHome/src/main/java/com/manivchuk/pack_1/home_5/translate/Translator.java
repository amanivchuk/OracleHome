package com.manivchuk.pack_1.home_5.translate;

import com.manivchuk.pack_1.home_5.ReadFile;
import com.manivchuk.pack_1.home_5.exception.DictionaryException;

import java.util.HashMap;
import java.util.Map;

public class Translator {
    private static HashMap<String, String> map = new HashMap<>();

    private static void addToMapSource(){
        String[] source = new String[0];
        try {
            source = ReadFile.getSb().toString().split("\n");
        } catch (DictionaryException e) {
            e.printStackTrace();
        }
        for(String s : source) {
            String[] regx = s.split(";-;");
             map.put(regx[0], regx[1]);
        }
    }

    public static Map<String, String> mapDictionary(){
        addToMapSource();
        return map;
    }
}
