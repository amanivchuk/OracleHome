package com.manivchuk.pack_1.home_6.part_1;

import java.io.*;

public class ConsoleHelper {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage(String str){
        System.out.println(str);
    }

    public static String readMessage() {
        try {
            String input = reader.readLine();
            return input;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void addBookToFile(String book){
        String file = "src/main/java/com/manivchuk/pack_1/home_6/files/archive";
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,true)))){
            writer.write(book);
            writeMessage("Done!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void serializObject(Object object) {
        String file = "src/main/java/com/manivchuk/pack_1/home_6/files/serializ";
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static Object unserilzObject(){
        String file = "src/main/java/com/manivchuk/pack_1/home_6/files/serializ";
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return ois.readObject();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

