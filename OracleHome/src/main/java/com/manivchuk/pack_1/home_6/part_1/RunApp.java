package com.manivchuk.pack_1.home_6.part_1;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class RunApp {
    private Map<Integer,Book> books = new HashMap<>();

    public void readFile(String file){
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))){
            String[] arr = null;
            String str;
            int count = 0;
            while((str = reader.readLine())!= null){
                if(str.equals(""))
                    break;
                arr = str.split("(\\w;)+");
                books.put(count++,new Book(arr[0],arr[1],Integer.parseInt(arr[2])));
                }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addBookInFile(){
        Book newBook = createBook();
        StringBuilder book = new StringBuilder();
        book.append(newBook.getAuthor()).append(";").append(newBook.getTitle()).append(";").append(newBook.getYear()).append("\n");
        ConsoleHelper.addBookToFile(book.toString());
        showAllBooks();
    }

    private Book createBook() {
        ConsoleHelper.writeMessage("Enter name of author:");
        String name = ConsoleHelper.readMessage();
        ConsoleHelper.writeMessage("Enter name of book:");
        String book = ConsoleHelper.readMessage();
        ConsoleHelper.writeMessage("Enter publish year:");
        int year = Integer.parseInt(ConsoleHelper.readMessage());
        Book newBook = new Book(book,name,year);
        return newBook;
    }

    public void showAllBooks(){
        for(Map.Entry map : books.entrySet()){
            System.out.println(map.getValue());
        }
    }

    public static void main(String[] args) {
        String file = "src/main/java/com/manivchuk/pack_1/home_6/files/archive";
        RunApp r = new RunApp();
        r.readFile(file);
        r.showAllBooks();
        r.addBookInFile();
    }
}
