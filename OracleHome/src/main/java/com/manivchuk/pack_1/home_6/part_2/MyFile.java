package com.manivchuk.pack_1.home_6.part_2;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class MyFile {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    public String path;

    public MyFile(String path) {
        this.path = path;
    }
    public String getPath() {return path;}

    public void createFile() throws IOException {
        System.out.println("Create file. Enter name file.");
        String nameFile = reader.readLine();
        boolean flag = new File(path+"/"+nameFile).createNewFile();
        if(!flag){
            System.out.println("File exists! Enter another name!");
            createFile();
        }
        System.out.println("File created!");
    }
    public void removeFile() throws IOException {
        System.out.println("Remove file. Enter name path file.");
        String file = reader.readLine();
        File f = new File(path+"/"+file);
        if(f.exists()){
            f.delete();
            System.out.println("File deleted!");
        }
    }
    public void renameFile() throws IOException {
        System.out.println("Rename file. Enter old name path file.");
        String file = reader.readLine();
        System.out.println("Enter new name path file.");
        String newFile = reader.readLine();

        File f = new File(path+"/"+file);
        File newNameFile = new File(path+"/"+newFile);
        if(f.exists()){
            f.renameTo(newNameFile);
            System.out.println("File renemed!");
        }
    }
    public void showDirectories(File folder){
        File[] folderEntire = folder.listFiles();
        for(File file : folderEntire){
            if(file.isDirectory()){
                System.out.println(": "+file.getName());
                showDirectories(file);
                continue;
            } else{
                System.out.println("->"+file.getName());
            }
        }

    }

    public static void main(String[] args) {
        MyFile myFile = new MyFile("src/main/java/com/manivchuk/pack_1/home_6/files");
        try {
            myFile.createFile();
            myFile.renameFile();
            myFile.showDirectories(new File(myFile.getPath()));
            myFile.removeFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
