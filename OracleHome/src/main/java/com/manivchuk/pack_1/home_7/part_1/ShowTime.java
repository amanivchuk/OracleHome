package com.manivchuk.pack_1.home_7.part_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class ShowTime {
    public class TimeR implements Runnable{
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()){
                LocalDateTime t = LocalDateTime.now();
                System.out.println(t.toLocalTime());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }
    public class TimeT extends Thread{
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()){
                System.out.println(new Date().toString());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public void start() {
        Thread timeR = new Thread(new TimeR());
        TimeT timeT = new TimeT();

        timeR.start();
        timeT.start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Enter different char for stop.");
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                try {
                    String str = reader.readLine();
                    if(str.matches("\\w*")) {
                        timeR.interrupt();
                        timeT.interrupt();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }
}
