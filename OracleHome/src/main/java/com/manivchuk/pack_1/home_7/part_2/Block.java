package com.manivchuk.pack_1.home_7.part_2;

public class Block implements Runnable {
    A a = new A () ;
    B b = new B();
    Block () {
        Thread.currentThread().setName("MainThread");
        Thread t = new Thread(this, "RacingThread");
        t.start () ;
        a.foo(b); // получить блокировку внутри этого потока.
        System.out.println("Назад в главный поток");
    }
    public void run() {
        b.bar(a); // получить блокировку b в другом потоке.
        System.out.println ("Назад в другой поток");
    }
    public static void main(String args[]) {
        new Block ();
    }
}
class A {
    synchronized void foo(B b) {
        String name = Thread.currentThread().getName();
        System.out.println(name + " вошел в A.foo");
        try {
            Thread.sleep(1000);
        }
        catch(Exception e) {
            System.out.println("А прерван");
        }
        System.out.println(name + " пытается вызвать B.lastO");
        b.last();
    }
    synchronized void last() {
        System.out.println("внутри A.last");
    }
}
class B {
    synchronized void bar(A a) {
        String name = Thread.currentThread().getName();
        System.out.println(name + " вошел в В.bar");
        try {
            Thread.sleep(1000);
        }
        catch(Exception e) {
            System.out.println("В прерван");
        }
        System.out.println(name + " пытается вызвать A.lastO");
        a.last();
    }
    synchronized void last() {
        System.out.println("внутри A.last");
    }
}

