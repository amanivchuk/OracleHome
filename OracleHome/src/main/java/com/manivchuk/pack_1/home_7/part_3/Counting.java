package com.manivchuk.pack_1.home_7.part_3;

import java.util.concurrent.TimeUnit;

public class Counting implements Runnable {
    public static int iCount = 0;
    public static int jCount = 0;

    public void iAdd(){
        iCount++;
        //System.out.println("i = " + iCount);
    }
    public void jAdd(){
        jCount++;
        //System.out.println("j = " + jCount);
    }

    @Override
    public void run() {
        while (true){
            iAdd();
            try {
                TimeUnit.MILLISECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            jAdd();
            System.out.println(Thread.currentThread().getName()+ " " + iCount + " == " + jCount);
        }
    }
}
