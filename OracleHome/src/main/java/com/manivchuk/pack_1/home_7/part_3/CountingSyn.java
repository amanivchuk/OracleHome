package com.manivchuk.pack_1.home_7.part_3;

import java.util.concurrent.TimeUnit;

public class CountingSyn implements Runnable{
    public static volatile int iCount = 0;
    public static volatile int jCount = 0;

    public synchronized void iAdd(){
        iCount++;
        //System.out.println("i = " + iCount);
    }
    public synchronized void jAdd(){
        jCount++;
        //System.out.println("j = " + jCount);
    }
    public synchronized void com(){

        iAdd();
        try {
            TimeUnit.MILLISECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        jAdd();
        System.out.println(Thread.currentThread().getName()+ " " + iCount + " == " + jCount);

    }
    @Override
    public void run() {
        while (true){
            com();
        }
    }
}