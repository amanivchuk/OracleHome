package com.manivchuk.pack_1.home_7.part_3;

public class Start {
    public static void main(String[] args) {
        Counting c = new Counting();
        Thread t1 = new Thread(c);
        Thread t2 = new Thread(c);
        Thread s1 = new Thread(new CountingSyn());
        Thread s2 = new Thread(new CountingSyn());
        t1.setName("T1");
        t2.setName("T2");
        s1.setName("S1");
        s2.setName("S2");
        t1.start();
        t2.start();
        s1.start();
        s2.start();
    }
}
