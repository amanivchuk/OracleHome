package com.manivchuk.pack_1.practise1.practise1_arrays;

public class ArrayProd {
    private int[] array;

    public ArrayProd(int[] array) {
        if(array == null)
            throw new NullPointerException();
        this.array = array;
    }
    public int prod(){
        if(array.length == 0)
            throw new NullPointerException();
        int result = 1;
        for(int i = 0; i < array.length;i++){
            result *= array[i];
        }
        return result;
    }
}
