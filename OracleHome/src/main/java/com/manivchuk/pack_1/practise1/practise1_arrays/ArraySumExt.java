package com.manivchuk.pack_1.practise1.practise1_arrays;

public class ArraySumExt {
    private static int[] array;

    public ArraySumExt(int[] array) {
        if (array == null)
            throw new NullPointerException();
        this.array = array;
    }
    public static int sum(){
        if(array.length == 0)
            throw new NullPointerException();
        int sum = 0;
        for(int i = 0; i < array.length;i++){
            sum += array[i];
        }
        System.out.println("Sum = " + sum);
        return sum;
    }
}
