package com.manivchuk.pack_1.practise1.practise1_students;

public class Exam {
    private String subjectName;
    private int rating;
    private String date;
    private int semestr;

    public Exam(String subjectName, int rating, String date, int semestr) {
        this.subjectName = subjectName;
        this.rating = rating;
        this.date = date;
        this.semestr = semestr;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getSemestr() {
        return semestr;
    }

    public void setSemestr(int semestr) {
        this.semestr = semestr;
    }
}
