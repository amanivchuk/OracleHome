package com.manivchuk.pack_1.practise1.practise1_students;

/**
 * Created by ASUS on 01.11.2016.
 */
public class ExamException extends Exception {
    public ExamException() {
    }

    public ExamException(String message) {
        super(message);
    }
}
