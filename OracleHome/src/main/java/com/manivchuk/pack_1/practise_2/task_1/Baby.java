package com.manivchuk.pack_1.practise_2.task_1;

/**
 * Created by ASUS on 05.11.2016.
 */
public class Baby {
    private int rank;
    private String maleName;
    private String femaleName;

    public Baby(int rank, String maleName, String femaleName) {
        this.rank = rank;
        this.maleName = maleName;
        this.femaleName = femaleName;
    }

    public int getRank() {
        return rank;
    }

    public String getMaleName() {
        return maleName;
    }

    public String getFemaleName() {
        return femaleName;
    }

    @Override
    public String toString() {
        return "rank=" + rank +
                ", maleName='" + maleName + '\'' +
                ", femaleName='" + femaleName + '\'';
    }
}
