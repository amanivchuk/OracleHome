package com.manivchuk.pack_1.practise_2.task_1;

import java.util.ArrayList;

/**
 * Created by ASUS on 05.11.2016.
 */
public class BabyBuff {
    private static ArrayList<Baby> babyList = new ArrayList<>();

    public static ArrayList<Baby> getBabyList() {
        return babyList;
    }

    public static void addBaby(Baby baby){
        babyList.add(baby);
    }

    public static void showAllBaby(){
        for(Baby baby : babyList){
            System.out.println(baby);
        }
    }
}
