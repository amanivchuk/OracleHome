package com.manivchuk.pack_1.practise_2.task_1;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ASUS on 05.11.2016.
 */
public class Parser {
    public ArrayList<String[]> read(String file){
        ArrayList<String[]> arrayList = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(new File(file).getAbsoluteFile()))){
            String str;
            while((str = br.readLine()) != null){
                //checkPattern(str);
                Pattern pattern = Pattern.compile("[^<td>]\\d+<.*>\\w+<.*>\\w+[^</td>]");
                Matcher matcher = pattern.matcher(str);
                while(matcher.find()){
                    String result = matcher.group().replaceAll("<[a-zA-Z\\s/]+>", " ");
                    String[] mas = result.split("  ");
                    arrayList.add(mas);
                    /*for(String s : mas) {
                        System.out.println(s);
                    }*/
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }


    public void createBaby(ArrayList<String[]> list) {
        for(String[] str : list){
            int id = Integer.parseInt(str[0]);
            String name1 = str[1];
            String name2 = str[2];
            //System.out.println("id= " + id + " name1 = " + name1 + " name2 = " + name2);
            Baby baby = new Baby(id, name1, name2);
            BabyBuff.addBaby(baby);
        }
    }
}
