package com.manivchuk.pack_1.practise_2.task_2;

import java.util.ArrayList;

public class AllProducts {
    private static ArrayList<Product> allProducts = new ArrayList<>();

    public static ArrayList<Product> getAllProducts() {
        return allProducts;
    }
    public static void addProduct(Product product){
        allProducts.add(product);
    }
    public static void showAllProduct(){
        for(Product pr : getAllProducts()){
            System.out.println(pr);
        }
    }
}
