package com.manivchuk.pack_1.practise_2.task_2;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

public class Parsing {
    public void read(String file){
        Document htmlFile = null;
        try {
            htmlFile = Jsoup.parse(new File(file),"ISO-8859-1");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Elements elements = htmlFile.select(".g-list-item");
        //System.out.println();
        for(Element elem : elements){
            String name = elem.select(".g-list-title").text();
            String href = elem.select(".g-list-title a").attr("href");
            String price = elem.select(".to-right.price.bg-red-ie-left-gray.bg-red-ie-left-white").text();
           // System.out.print("name = " + name + " price = " + price + " href = " + href);
            Elements characters = elem.select(".cell.g-list-info-describe");
            for(Element charac : characters){
                String discribe = charac.select(".p-describe-i-value").text();
               // System.out.print(" diagonal = " + discribe);
                AllProducts.addProduct(new Product(name,href,price,discribe));
            }
            //System.out.println();
        }
    }
}
