package com.manivchuk.pack_1.practise_2.task_2;

public class Product {
    private String name;
    private String href;
    private String price;
    private String discribe;

    public Product(String name, String href, String price, String discribe) {
        this.name = name;
        this.href = href;
        this.price = price;
        this.discribe = discribe;
    }

    @Override
    public String toString() {
        return "name='" + name +
                ", href='" + href +
                ", price='" + price +
                ", discribe='" + discribe;
    }
}