package com.manivchuk.pack_1.practise_2.task_2_elmir;

import java.util.ArrayList;

public class LapTop {
    private String name;
    private String description;
    private String price;
    private static ArrayList<LapTop> allLapTop = new ArrayList<>();

    public LapTop(String name, String description, String price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }
    public static void addLapTop(LapTop lapTop){
        allLapTop.add(lapTop);
    }
    public static void showAllLapTop(){
        for(LapTop l : allLapTop){
            System.out.println(l);
        }
    }

    @Override
    public String toString() {
        return "name= " + name +
                ", description= " + description  +
                ", price= " + price;
    }
}
