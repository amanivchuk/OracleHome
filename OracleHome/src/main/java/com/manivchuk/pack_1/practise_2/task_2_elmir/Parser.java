package com.manivchuk.pack_1.practise_2.task_2_elmir;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class Parser {
    public static void parse(String html){
        Document doc = Jsoup.parse(html);
        Elements elem = doc.select(".item");
        for(Element e : elem){
            String name = e.select(".name").text();
            String description = e.select(".description").text();
            String price = e.select(".buy_button").text();
            //System.out.println("name = " + name + " price = "+ price + " description = " + description);
            LapTop.addLapTop(new LapTop(name,price,description));
        }
    }
}
