package com.manivchuk.pack_1.practise_2.task_2_elmir;

import java.io.*;

public class ReadFile {
    public static String readFile(String file, String charset){
        StringBuilder sb = new StringBuilder();
        String str;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),charset))) {
              while((str = reader.readLine()) != null){
                  sb.append(str);
              }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
