package com.manivchuk.pack_1.practise_3;

import java.util.HashMap;
import java.util.Map;

public class CinemaHall {
    private static HashMap<String, Places> allPlaces = new HashMap<>();
    static {
        CinemaHall cn = new CinemaHall();
        cn.addHallAndCountPlaces("Red",5);
        cn.addHallAndCountPlaces("Brown",8);
    }

    public void addHallAndCountPlaces(String nameOfHall, int countPlaces){
        Places places = new Places(countPlaces);
        allPlaces.put(nameOfHall, places);
    }
    public static Places getInformOfHall(String nameOfHall){
        return allPlaces.get(nameOfHall);
    }

    public void countPlaces(String nameOfHall){
         allPlaces.get(nameOfHall).CountOfPlaces();
     }

    public void buyPlace(String nameOfHall, int numberPlace){
        allPlaces.get(nameOfHall).takePlace(numberPlace);
    }

    public static void showNameOfHallAndInformAboutPlace(){
        for(Map.Entry e : allPlaces.entrySet()){
            System.out.println(e.getKey());
            Places p = (Places) e.getValue();
            p.showPlaces();
        }
    }
}
