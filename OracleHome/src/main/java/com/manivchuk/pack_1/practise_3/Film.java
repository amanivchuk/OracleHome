package com.manivchuk.pack_1.practise_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Film {
    private static HashMap<String, Film> allFilms = new HashMap<>();
    private String filmName;
    private double duration;
    private String description;
    static {
        allFilms.put("Titanic", new Film("Titanic",120, "Good film."));
        allFilms.put("Pirates of Caribian", new Film("Pirates of Caribian",140, "Very good film."));
        allFilms.put("Man in black", new Film("Man in black", 155, "Film about future."));
    }

    public Film() {
    }

    public Film(String filmName, double duration, String description) {
        this.filmName = filmName;
        this.duration = duration;
        this.description = description;
    }

    public void addFilm(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        boolean flag = false;
        while(!flag){
            try {
                System.out.println("Enter new name of film.");
                String name = reader.readLine();
                System.out.println("Enter duration of film.");
                double lenFilm = Double.parseDouble(reader.readLine());
                System.out.println("Enter description of film.");
                String descrip = reader.readLine();
                allFilms.put(name, new Film(name, lenFilm, descrip));

                System.out.println("Would you like add new film: y or n");
                String answer = reader.readLine();
                if(answer.equals("n"))
                    flag = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public void showAllFilmInform(){
        for(Map.Entry e : allFilms.entrySet()){
            System.out.println(e.getValue());
        }
    }
    public static void showNameFilm(){
        for(Map.Entry e : allFilms.entrySet()){
            System.out.print(e.getKey() + "; ");
        }
        System.out.println();
    }
    public static Film getInformAboutFilm(String nameOfFilm){
        return allFilms.get(nameOfFilm);
    }

    @Override
    public String toString() {
        return  filmName + ", Duration = " + duration + " min." +
                " Description = '" + description;
    }
}
