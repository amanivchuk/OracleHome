package com.manivchuk.pack_1.practise_3.GenericStoragePack;

public interface GenericStorage<K,V> {
    K add(V value);
    V get(K id);
    V delete(K id);
    boolean update(K id, V value);
}
