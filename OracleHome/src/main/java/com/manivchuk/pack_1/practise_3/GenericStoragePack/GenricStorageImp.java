package com.manivchuk.pack_1.practise_3.GenericStoragePack;

import lombok.Data;

import java.util.*;
import java.util.ArrayList;

public class GenricStorageImp<K extends Long, V> implements GenericStorage<K, V> {
    private K id = (K)new Long(0);
    private List<Node<K,V>> nodeList = new ArrayList<>();
    private class Node<K,V>{
        K id;
        V value;
        public Node(K id, V value){
            setId(id);
            setValue(value);
        }

        public K getId() {
            return id;
        }
        public void setId(K id) {
            this.id = id;
        }
        public V getValue() {
            return value;
        }
        public void setValue(V value) {
            this.value = value;
        }
    }
    @Override
    public K add(V value) {
        nodeList.add(new Node<>(id,value));
        id = (K)Long.valueOf(id.longValue()+1);
        return id;
    }

    @Override
    public V get(K id) {
        for (Node<K,V> kvNode : nodeList) {
            if (kvNode.getId().equals(id)){
                return kvNode.getValue();
            }
        }
        return null;
    }

    @Override
    public V delete(K id) {
        V value = null;
        for (int i = 0; i < nodeList.size(); i++) {
            if (nodeList.get(i).getId().equals(id)){
                value = nodeList.get(i).getValue();
                nodeList.remove(i);
            }
        }
        return value;
    }

    @Override
    public boolean update(K id, V value) {
        return false;
    }
}
