package com.manivchuk.pack_1.practise_3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Places {
    private boolean[] places;

    public Places(int sizeOfPlaces) {
        places = new boolean[sizeOfPlaces];
    }
    public void CountOfPlaces(){
        System.out.println("Count of place in " + places.length);
    }

    public boolean takePlace(int numberPlace){
        if(numberPlace < 0 && numberPlace > places.length)
            return false;

        if(places[numberPlace] != true) {
            places[numberPlace] = true;
            return true;
        }else return false;
    }

    public boolean removePlace(int numberPlace){
        if(numberPlace < 0 && numberPlace > places.length)
            return false;
        if(places[numberPlace] != false){
            places[numberPlace] = false;
            return true;
        }else return false;
    }
    public void showPlaces(){
        TreeMap<Integer,String> allPlaces = new TreeMap<>();
        int numberOfPlace = 0;
        for(Boolean place : places){
            if(place == false)
                allPlaces.put(numberOfPlace++,"Free");
            else
                allPlaces.put(numberOfPlace++, "Busy");
        }
        for(Map.Entry e : allPlaces.entrySet()){
            System.out.print(e.getKey() + " - " + e.getValue() + "; ");
        }
        System.out.println();
    }
}
