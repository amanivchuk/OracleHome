package com.manivchuk.pack_1.practise_3;

import com.manivchuk.pack_1.practise_3.command.CommandExecutor;
import com.manivchuk.pack_1.practise_3.exeption.InterruptOperationException;
import com.manivchuk.pack_1.practise_3.user.ConsoleHelper;
import com.manivchuk.pack_1.practise_3.user.Operation;

import java.util.Locale;

public class Start {
    public static void main(String[] args) {
        try {
            Locale.setDefault(Locale.ENGLISH);
            //CommandExecutor.execute(Operation.LOGIN);
            Operation operation;
            do{
                operation = ConsoleHelper.askOperation();
                CommandExecutor.execute(operation);
            }while (operation != operation.EXIT);
        } catch (InterruptOperationException e) {
            e.printStackTrace();
        }
    }
}
