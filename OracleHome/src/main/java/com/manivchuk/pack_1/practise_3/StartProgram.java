package com.manivchuk.pack_1.practise_3;

public class StartProgram {
    static {
        CinemaHall cn = new CinemaHall();
        cn.addHallAndCountPlaces("Red",5);
        cn.addHallAndCountPlaces("Brown",8);
    }
    public static void main(String[] args) {
        /*Film film = new Film();
        film.addFilm();
        film.showAllFilmInform();*/

        /*System.out.println("Adding hall and count of places.");
        CinemaHall cn = new CinemaHall();
        cn.addHallAndCountPlaces("Red",5);
        cn.addHallAndCountPlaces("Brown",8);

        cn.countPlaces("Red");
        cn.countPlaces("Brown");

        CinemaHall.showNameOfHallAndInformAboutPlace();

        System.out.println();

        cn.buyPlace("Red",3);
        cn.buyPlace("Red",4);
        CinemaHall.showNameOfHallAndInformAboutPlace();
*/

        Ticket ticket = new Ticket();
        ticket.buyTicket();
    }
}
