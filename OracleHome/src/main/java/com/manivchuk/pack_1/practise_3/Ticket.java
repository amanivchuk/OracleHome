package com.manivchuk.pack_1.practise_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ticket {
    private Film film;
    private User user;
    private Places place;

    public Film getFilm() {
        return film;
    }

    public User getUser() {
        return user;
    }

    public Places getPlace() {
        return place;
    }

    public void buyTicket(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter name and surname.");
            User user = new User(reader.readLine(), reader.readLine());

            System.out.println("Enter name of film");
                Film.showNameFilm();
            String nameOfFilm = reader.readLine();
            Film film = Film.getInformAboutFilm(nameOfFilm);

            System.out.println("Choose hall nad place.");
            CinemaHall.showNameOfHallAndInformAboutPlace();
            System.out.println("Enter name of hall : ");
            String nameHall = reader.readLine();
            System.out.println("Enter number of place : ");
            int numberPlace = Integer.parseInt(reader.readLine());
            Places p = CinemaHall.getInformOfHall(nameHall);
            p.takePlace(numberPlace);

            showTicket(user, film, nameHall, numberPlace);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void showTicket(User user, Film film, String nameHall, int numberPlace) {
        System.out.println(user + " " +  film + " Hall: " + nameHall + " Place: " + numberPlace);
    }
}
