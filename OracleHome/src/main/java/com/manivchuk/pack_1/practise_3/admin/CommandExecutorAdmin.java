package com.manivchuk.pack_1.practise_3.admin;

import com.manivchuk.pack_1.practise_3.command.*;
import com.manivchuk.pack_1.practise_3.exeption.InterruptOperationException;

import java.util.HashMap;
import java.util.Map;

public class CommandExecutorAdmin {
    private static Map<OperationAdmin, Command> map;
    static {
        map = new HashMap<>();
        map.put(OperationAdmin.ADDFILM, new AddFilmCommand());
        map.put(OperationAdmin.SHOW_ALL_FILM, new ShowFilmCommand());
        map.put(OperationAdmin.ADD_HALL_AND_PLACES, new AddHallAndCountPlaceCommand());
        map.put(OperationAdmin.INFORM_PLACES, new InformPlacesCommand());
        map.put(OperationAdmin.LOGOUT, new LogoutCommand());
    }

    public static final void execute(OperationAdmin operation) throws InterruptOperationException {
        map.get(operation).execute();
    }
}
