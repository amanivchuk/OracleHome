package com.manivchuk.pack_1.practise_3.admin;

public enum OperationAdmin {
    ADDFILM(0), ADD_HALL_AND_PLACES(1), INFORM_PLACES(2), SHOW_ALL_FILM(3), LOGOUT(4);

    int id;
    OperationAdmin(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public static OperationAdmin getAllowedOperationAdminByOrdinal(Integer i){
        switch (i){
            case 0 : return OperationAdmin.ADDFILM;
            case 1 : return OperationAdmin.ADD_HALL_AND_PLACES;
            case 2 : return OperationAdmin.INFORM_PLACES;
            case 3 : return OperationAdmin.SHOW_ALL_FILM;
            case 4 : return OperationAdmin.LOGOUT;
            default : throw new IllegalArgumentException();
        }
    }
}
