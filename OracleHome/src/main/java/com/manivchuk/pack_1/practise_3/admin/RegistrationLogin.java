package com.manivchuk.pack_1.practise_3.admin;

import com.manivchuk.pack_1.practise_3.user.ConsoleHelper;

import java.util.Map;
import java.util.TreeMap;

public class RegistrationLogin {
    private static Map<String, String> users = new TreeMap<>();
    static {
        users.put("admin", "admin");
        users.put("root", "root");
    }
    public boolean loginIn(){
        boolean loginFlag = false;
        try {
            ConsoleHelper.writeMessage("Enter login");
            String login = ConsoleHelper.readString();
            ConsoleHelper.writeMessage("Enter password");
            String password = ConsoleHelper.readString();

            for(Map.Entry m : users.entrySet()){
                if (m.getKey().equals(login) && m.getValue().equals(password)){
                    loginFlag = true;
                    ConsoleHelper.writeMessage("Welcome " + login + "!\n");
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return loginFlag;
    }
}
