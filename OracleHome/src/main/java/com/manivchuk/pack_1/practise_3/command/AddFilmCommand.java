package com.manivchuk.pack_1.practise_3.command;

import com.manivchuk.pack_1.practise_3.Film;
import com.manivchuk.pack_1.practise_3.exeption.InterruptOperationException;
import com.manivchuk.pack_1.practise_3.user.ConsoleHelper;

public class AddFilmCommand implements Command {
    @Override
    public void execute() throws InterruptOperationException {
        Film film = new Film();
        film.addFilm();
        ConsoleHelper.writeMessage("");
    }
}
