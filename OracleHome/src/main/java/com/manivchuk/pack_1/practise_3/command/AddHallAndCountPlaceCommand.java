package com.manivchuk.pack_1.practise_3.command;

import com.manivchuk.pack_1.practise_3.CinemaHall;
import com.manivchuk.pack_1.practise_3.exeption.InterruptOperationException;
import com.manivchuk.pack_1.practise_3.user.ConsoleHelper;

public class AddHallAndCountPlaceCommand implements Command {
    @Override
    public void execute() throws InterruptOperationException {
        CinemaHall ch = new CinemaHall();
        try {
            ConsoleHelper.writeMessage("Enter name of hall.");
            String hallName = ConsoleHelper.readString();
            ConsoleHelper.writeMessage("Enter count place.");
            int countPlace = Integer.parseInt(ConsoleHelper.readString());
            ConsoleHelper.writeMessage("You create " + hallName + " hall, and " + countPlace + " places. \n");
            ch.addHallAndCountPlaces(hallName, countPlace);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
