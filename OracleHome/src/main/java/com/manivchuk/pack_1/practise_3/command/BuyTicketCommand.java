package com.manivchuk.pack_1.practise_3.command;

import com.manivchuk.pack_1.practise_3.Ticket;
import com.manivchuk.pack_1.practise_3.exeption.InterruptOperationException;
import com.manivchuk.pack_1.practise_3.user.ConsoleHelper;

public class BuyTicketCommand implements Command {
    @Override
    public void execute() throws InterruptOperationException {
        Ticket ticket = new Ticket();
        ticket.buyTicket();
        ConsoleHelper.writeMessage("");
    }
}
