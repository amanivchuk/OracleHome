package com.manivchuk.pack_1.practise_3.command;

import com.manivchuk.pack_1.practise_3.exeption.InterruptOperationException;

public interface Command {
    void execute() throws InterruptOperationException;
}
