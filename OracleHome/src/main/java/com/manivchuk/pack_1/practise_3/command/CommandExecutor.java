package com.manivchuk.pack_1.practise_3.command;

import com.manivchuk.pack_1.practise_3.user.Operation;
import com.manivchuk.pack_1.practise_3.exeption.InterruptOperationException;

import java.util.HashMap;
import java.util.Map;

public class CommandExecutor {
    private static Map<Operation, Command> map;
    static {
        map = new HashMap<>();
        map.put(Operation.LOGIN, new LoginCommand());
        //map.put(OperationAdmin.ADDFILM, new AddFilmCommand());
        map.put(Operation.BUYTICKET, new BuyTicketCommand());
        map.put(Operation.PLACES, new PlacesCommand());
        map.put(Operation.SHOW_FILM, new ShowFilmCommand());
        map.put(Operation.EXIT, new ExitCommand());
    }

    public static final void execute(Operation operation) throws InterruptOperationException {
        map.get(operation).execute();
    }
}
