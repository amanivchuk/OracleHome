package com.manivchuk.pack_1.practise_3.command;

import com.manivchuk.pack_1.practise_3.admin.OperationAdmin;
import com.manivchuk.pack_1.practise_3.user.ConsoleHelper;

public class ConsoleHelperAdmin {
    public static OperationAdmin askOperation(){
        ConsoleHelper.writeMessage("Choose number of operation for Admin:");
        for (OperationAdmin o : OperationAdmin.values()){
            ConsoleHelper.writeMessage(o.getId() + ": " +o.toString());
        }
        OperationAdmin operation;
        while(true){
            try {
                int selection = Integer.parseInt(ConsoleHelper.readString());
                operation = OperationAdmin.getAllowedOperationAdminByOrdinal(selection);
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return operation;
    }
}
