package com.manivchuk.pack_1.practise_3.command;

import com.manivchuk.pack_1.practise_3.admin.OperationAdmin;
import com.manivchuk.pack_1.practise_3.admin.CommandExecutorAdmin;
import com.manivchuk.pack_1.practise_3.admin.RegistrationLogin;
import com.manivchuk.pack_1.practise_3.exeption.InterruptOperationException;
import com.manivchuk.pack_1.practise_3.user.ConsoleHelper;

import java.util.Locale;

public class LoginCommand implements Command {
    RegistrationLogin rl = new RegistrationLogin();
    @Override
    public void execute() throws InterruptOperationException {
        if(rl.loginIn() == true){
            startAdminOperations();
        }else {
            ConsoleHelper.writeMessage("Incorrect login or password!");
            CommandExecutorAdmin.execute(OperationAdmin.LOGOUT);
        }
    }

    private void startAdminOperations() {
        try {
            Locale.setDefault(Locale.ENGLISH);
            OperationAdmin operationAdmin;
            do{
                operationAdmin = ConsoleHelperAdmin.askOperation();
                CommandExecutorAdmin.execute(operationAdmin);
            }while (operationAdmin != operationAdmin.LOGOUT);
        } catch (InterruptOperationException e) {
            e.printStackTrace();
        }
    }
}
