package com.manivchuk.pack_1.practise_3.command;

import com.manivchuk.pack_1.practise_3.CinemaHall;
import com.manivchuk.pack_1.practise_3.exeption.InterruptOperationException;
import com.manivchuk.pack_1.practise_3.user.ConsoleHelper;

public class PlacesCommand  implements Command{
    @Override
    public void execute() throws InterruptOperationException {
        CinemaHall.showNameOfHallAndInformAboutPlace();
        ConsoleHelper.writeMessage("");
    }
}
