package com.manivchuk.pack_1.practise_3.exeption;

public class InterruptOperationException extends Exception {

    public InterruptOperationException() {
    }

    public InterruptOperationException(String message) {
        super(message);
    }
}
