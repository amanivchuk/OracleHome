package com.manivchuk.pack_1.practise_3.user;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleHelper {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage(String message){
        System.out.println(message);
    }

    public static String readString() throws InterruptedException {
        try {
            String input = reader.readLine();
            if(input.equalsIgnoreCase(String.valueOf(Operation.EXIT)))
                throw new InterruptedException();
            return input;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Operation askOperation(){
        writeMessage("Choose number of operation:");
        for (Operation o : Operation.values()){
            writeMessage(o.getId() + ": " +o.toString());
        }
        Operation operation;
        while(true){
            try {
                int selection = Integer.parseInt(readString());
                operation = Operation.getAllowedOperationByOrdinal(selection);
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return operation;
    }
}
