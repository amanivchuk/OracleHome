package com.manivchuk.pack_1.practise_3.user;

public enum Operation {
    LOGIN(0), PLACES(1), BUYTICKET(2), SHOW_FILM(3), EXIT(4);

    private int id;

    Operation(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public static Operation getAllowedOperationByOrdinal(Integer i){
        switch (i){
            case 0 : return Operation.LOGIN;
            case 1 : return Operation.PLACES;
            case 2 : return Operation.BUYTICKET;
            case 3 : return Operation.SHOW_FILM;
            case 4 : return Operation.EXIT;
            default : throw new IllegalArgumentException();
        }
    }
}
