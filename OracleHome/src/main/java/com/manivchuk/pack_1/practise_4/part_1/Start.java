package com.manivchuk.pack_1.practise_4.part_1;
import java.util.*;

public class Start {
    public static void main(String[] args) {
        List<Student> listStudent = new ArrayList<>();
        listStudent.add(new Student("Ivan","Ivanov",2));
        listStudent.add(new Student("Jora","Vartanov",3));
        listStudent.add(new Student("Gosha","Petrov",3));
        listStudent.add(new Student("Alex","Alexandr",1));

        StudentUtils.printStudents(listStudent,3);

        System.out.println("==========================");
        List<Student> sorter = StudentUtils.sortStudent(listStudent);
        sorter.forEach(System.out::println);
    }
}
