package com.manivchuk.pack_1.practise_4.part_1;

import java.util.Map;
import java.util.*;

public class StudentUtils {
    public static Map<String, Student> createMapFromList(List<Student> students){
        Map<String,Student> studentMap = new TreeMap<>();
        for(Student student : students){
            studentMap.put(student.getFirstName()+student.getLastName(),student);
        }
        return studentMap;
    }
    public static void printStudents(List<Student> students, int course){
        Iterator<Student> iterator = students.iterator();
        while (iterator.hasNext()){
            Student st = iterator.next();
            if(st.getCourse() == course){
                System.out.println(st);
            }
        }
    }
    public static List<Student> sortStudent(List students){
        List<Student> sortStudent = new ArrayList<>(students);
        Collections.sort(sortStudent, (o1, o2) -> {
            int res = (o1.getFirstName().compareToIgnoreCase(o2.getFirstName())) &
                    (o1.getLastName().compareToIgnoreCase(o2.getLastName()));
            return res;
        });
        return sortStudent;
    }
}
