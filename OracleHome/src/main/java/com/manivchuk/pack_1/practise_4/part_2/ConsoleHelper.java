package com.manivchuk.pack_1.practise_4.part_2;

import com.sun.javafx.collections.MappingChange;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class ConsoleHelper {
    public static void writeMessage(String str){
        System.out.println(str);
    }

    public static String readMessage(){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = null;
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
    public static void choseSortMethod(Map<String,Integer> map){
        writeMessage("Enter method for sort: key or value");
        String resulr = readMessage();
        if(resulr.equalsIgnoreCase("key")){
            SortEnum.runSortMethod(SortEnum.ByKey, map);
        }
        if(resulr.equalsIgnoreCase("value")){
            SortEnum.runSortMethod(SortEnum.ByValue, map);
        }
    }
}
