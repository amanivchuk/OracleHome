package com.manivchuk.pack_1.practise_4.part_2;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Finder {
    public static String readFile(String file){
        StringBuilder sb = new StringBuilder();
        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            String str;
            while((str = br.readLine()) != null){
                sb.append(str).append(" ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static Map<String, Integer> findWord(String words){
        Map<String,Integer> mapWords = new HashMap<>();
        String[] allWords = (words + " ").split("\\p{P}?[ \\t\\n\\r]+");

        for(String str : allWords){
            if(mapWords.containsKey(str)){
                int i = mapWords.get(str);
                mapWords.replace(str, ++i);
            }else {
                mapWords.put(str, 1);
            }
        }
        return mapWords;
    }
}
