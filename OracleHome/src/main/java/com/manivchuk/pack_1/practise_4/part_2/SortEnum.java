package com.manivchuk.pack_1.practise_4.part_2;

import java.util.Map;

public enum SortEnum {
    ByValue(0), ByKey(1);

    int id;

    SortEnum(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    public static void runSortMethod(SortEnum sr, Map<String,Integer> map){
        switch (sr){
            case ByKey: Sortering.sortByKey(map);
                break;
            case ByValue: Sortering.sortByValue(map);
                break;
        }
    }
}
