package com.manivchuk.pack_1.practise_4.part_2;

import java.util.*;

public class Sortering {

    public static void sortByValue(Map<String, Integer> map){
        Map<String, Integer> m = new HashMap<>(map);

        List<Map.Entry<String,Integer>> list = new ArrayList(m.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String,Integer>>() {
            @Override
            public int compare(Map.Entry<String,Integer> o1, Map.Entry<String,Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        for(Map.Entry<String,Integer> m1 : list){
            System.out.println(m1.getKey() + " - " + m1.getValue());
        }
    }

    public static void sortByKey(Map<String,Integer> map){
        TreeMap<String,Integer> sortMap = new TreeMap<>(map);
        for(Map.Entry<String,Integer> m : sortMap.entrySet()){
            System.out.println(m.getKey() + " - " + m.getValue());
        }
    }
}
