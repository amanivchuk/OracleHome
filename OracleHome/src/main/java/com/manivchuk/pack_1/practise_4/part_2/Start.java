package com.manivchuk.pack_1.practise_4.part_2;

import java.util.*;

public class Start {
    public static void main(String[] args) {
        Map<String, Integer> map;
        String path = "src/main/java/com/manivchuk/pack_1/practise_4/part_2/Romeo.txt";

        String words = Finder.readFile(path);
        map = Finder.findWord(words);

        /*for(Map.Entry m : map.entrySet()){
            System.out.println(m.getKey() + " - " + m.getValue());
        }*/

        ConsoleHelper.choseSortMethod(map);
    }

}
