package com.manivchuk.pack_1.practise_5.part_1_2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class RandomGenerator {
    public static void randomGeneratorInt(String file){
        Random random = new Random(99);
        StringBuilder sb = new StringBuilder();
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(file)))) {
            for(int i = 0;i < 500; i++){
                sb.append(random.nextInt(1000)).append("\n");
            }
            bw.write(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
