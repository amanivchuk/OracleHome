package com.manivchuk.pack_1.practise_5.part_1_2;

import java.io.*;
import java.util.*;

public class Sorter {
    public static void sortFile(String file) {
        Set<Integer> set = new HashSet<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
             BufferedWriter bw = new BufferedWriter(new FileWriter("src/main/java/com/manivchuk/pack_1/practise_5/files/SorterResult.txt"))) {
            String str;
            while ((str = br.readLine()) != null) {
                set.add(Integer.parseInt(str));
            }

            ArrayList<Integer> listForSort = new ArrayList(set);
            Collections.sort(listForSort, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o1.compareTo(o2);
                }
            });

            for(Integer res : listForSort){
                bw.write(res + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
