package com.manivchuk.pack_1.practise_5.part_3;

import java.io.*;
import java.util.*;

public class HightRaiting {
   private static List<Student> listStudents = new ArrayList<>();

    public static void readStudentFromFile(String file) {
        listStudents = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            String str;
            while((str = reader.readLine()) != null){
                String[] student = str.split(" = ");
                listStudents.add(new Student(student[0],Integer.parseInt(student[1])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void studentWithRaiting90(){
        listStudents.stream().filter(st -> st.getRaiting() >= 90).forEach(System.out::println);
    }
}
