package com.manivchuk.pack_1.practise_5.part_4;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class ChangeWorld {
    static StringBuilder sb = new StringBuilder();
    private static ArrayList<String> list = new ArrayList<>();
    public static void readFile(String file) {
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            String str;
            while((str = reader.readLine()) != null){
                ArrayList<String> listWords = new ArrayList<>(Arrays.asList(str.split(" ")));
                String n = listWords.get(0);
                listWords.set(0,listWords.get(listWords.size()-1));
                listWords.set(listWords.size()-1,n);
                for (String s : listWords){
                    sb.append(s).append(" ");
                }
            }
            System.out.println(sb.toString());


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
