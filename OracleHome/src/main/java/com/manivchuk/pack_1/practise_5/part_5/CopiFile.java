package com.manivchuk.pack_1.practise_5.part_5;

import java.io.*;

public class CopiFile {
    public static void copyFileBuff(String from, String to){
        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(new File(from)));
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(to)));
            int i = 0;
            byte[] buff = new byte[1024];

            long start = System.nanoTime();

            while ((i = bis.read(buff)) != -1){
                bos.write(buff,0,i);
            }
            long end = System.nanoTime();
            System.out.println("Programm has worked with buffer = " + (end-start) + " ms.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copyFile(String from, String to){
        try {
            FileInputStream fis = new FileInputStream(from);
            FileOutputStream fos = new FileOutputStream(to);
            int i = 0;
            byte[] buff = new byte[1024];

            long start = System.currentTimeMillis();

            while ((i = fis.read(buff)) != -1){
                fos.write(buff,0,i);
            }
            long end = System.currentTimeMillis();
            System.out.println("Programm has worked without buff = " + (end-start) + " ms.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
