package com.manivchuk.pack_1.practise_5.part_5;

public class Start {
    public static void main(String[] args) {
        String from = "src/main/java/com/manivchuk/pack_1/practise_5/files/pick.jpg";
        String to = "src/main/java/com/manivchuk/pack_1/practise_5/files/pick1.jpg";
        CopiFile.copyFileBuff(from, to);
        CopiFile.copyFile(from,to);
    }
}
