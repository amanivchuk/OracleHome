package com.manivchuk.pack_1.practise_5.part_6;

import java.io.Serializable;
import java.util.ArrayList;

public class Group implements Serializable {
    private ArrayList<Student> listStudent = new ArrayList<>();
    public void addStudentToList(){
        listStudent.add(new Student("Ivan",90));
        listStudent.add(new Student("Jora",95));
        listStudent.add(new Student("Bory",80));
    }
    public void showStudent(){
        listStudent.forEach(System.out::println);
    }
}
