package com.manivchuk.pack_1.practise_5.part_6;

import java.io.*;

public class SerializGroup {
    public static void serializableGroup(String to){
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(to));
            Group group = new Group();
            group.addStudentToList();
            oos.writeObject(group);
            System.out.println("Serializable done!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void unserializableGroup(String from){
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(from));
            Group group = (Group)ois.readObject();
            group.showStudent();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
