package com.manivchuk.pack_1.practise_5.part_6;

public class Start {
    public static void main(String[] args) {
        String file = "src/main/java/com/manivchuk/pack_1/practise_5/files/serializ";
        SerializGroup.serializableGroup(file);
        SerializGroup.unserializableGroup(file);
    }
}
