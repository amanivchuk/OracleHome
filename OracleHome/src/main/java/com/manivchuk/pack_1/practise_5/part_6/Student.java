package com.manivchuk.pack_1.practise_5.part_6;

import java.io.Serializable;

public class Student implements Serializable{
    private String name;
    private int raiting;

    public Student(String name, int raiting) {
        this.name = name;
        this.raiting = raiting;
    }

    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public int getRaiting() {return raiting;}
    public void setRaiting(int raiting) {this.raiting = raiting;}

    @Override
    public String toString() {
        return "name=" + name +
                ", raiting=" + raiting;
    }
}
