package com.manivchuk.pack_1.practise_6.part_1;

import java.util.concurrent.TimeUnit;

public class MyThread implements Runnable {
    @Override
    public void run() {
        while(!Thread.interrupted()){
            System.out.println(Thread.currentThread().getName());
            sleep(500);
        }
    }

    private void sleep(int i) {
        try {
            TimeUnit.MILLISECONDS.sleep(i);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
