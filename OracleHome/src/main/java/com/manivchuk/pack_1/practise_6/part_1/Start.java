package com.manivchuk.pack_1.practise_6.part_1;

import java.util.concurrent.TimeUnit;

public class Start {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new MyThread());
        thread.setName("imp_Runnable");

        Thread thread2 = new MyThreadT();
        thread2.setName("ext_Thread");

        thread.start();
        thread2.start();

        TimeUnit.SECONDS.sleep(5);

        thread.interrupt();
        thread2.interrupt();

    }
}
