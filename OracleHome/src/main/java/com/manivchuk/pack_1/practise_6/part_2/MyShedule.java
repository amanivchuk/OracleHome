package com.manivchuk.pack_1.practise_6.part_2;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MyShedule implements Runnable {
    Map<Long, String> map;

    public void setMap(Map<Long,String> maps){
        map = maps;
    }


    @Override
    public void run() {
        for(Map.Entry<Long,String> m : map.entrySet()){
            long time = m.getKey();
            String message = m.getValue();
            System.out.println(message);
            sleep(time);
        }
        System.out.println("Bye!!!");
    }

    private void sleep(long time) {
        try {
            TimeUnit.MILLISECONDS.sleep(time);
        } catch (InterruptedException e) {
            System.out.println("----->sleep<-----");
        }
    }
}
