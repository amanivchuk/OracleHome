package com.manivchuk.pack_1.practise_6.part_2;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Start {
    private static Map<Long,String> map = new HashMap<>();
    static {
        map.put(300L,"Hello_300");
        map.put(600L,"Hello_600");
        map.put(9000L,"Hello_9000");
        map.put(5000L, "Hello_5000");
        map.put(200L, "Hello_200");
    }
    public static void main(String[] args) {


        ExecutorService exec = Executors.newCachedThreadPool();
        MyShedule myShedule = new MyShedule();
        myShedule.setMap(map);
        exec.submit(myShedule);
        exec.shutdown();


    }
}
