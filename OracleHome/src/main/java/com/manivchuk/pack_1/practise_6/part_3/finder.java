package com.manivchuk.pack_1.practise_6.part_3;

import java.io.*;

public class finder {
    private String searchingFile;

    public void runProg(){
        searchingFile = nameFileForFind();
        System.out.println("Searching...");
        findFile(pathForsearchingFile());
    }

    private void findFile(File f){
        File[] folderEntire = f.listFiles();
        for(File file : folderEntire){
            if(file.isDirectory()){

              new Thread(new Runnable() {
                  @Override
                  public void run() {
                      findFile(file);
                  }
              }).start();

            } else{
                if(file.getName().contains(searchingFile)){
                    writeLog(file.getAbsolutePath());
                    //System.out.println("->"+file.getName());
                   // System.out.println("->"+file.getAbsolutePath());
                }
            }
        }
    }

    public String nameFileForFind(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter name of file which searching!");
            String fileName = reader.readLine();
            return fileName;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public File pathForsearchingFile(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter name of path where searching!");
            String path = reader.readLine();
            return new File(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public synchronized void writeLog(String log){
        String pathLog = "src/main/java/com/manivchuk/pack_1/practise_6/files/logs.txt";
        try {
            PrintWriter write = new PrintWriter(new FileWriter(pathLog,true));
                //System.out.println("====================>>> "+ log);
            write.write(log + "\n");
            write.flush();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
