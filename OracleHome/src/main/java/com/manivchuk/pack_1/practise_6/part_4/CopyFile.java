package com.manivchuk.pack_1.practise_6.part_4;

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.file.Files;

public class CopyFile {

    public File enterDerictory(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter name of path for copy file.");
            String path = reader.readLine();
            return new File(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public void findFile(File f){
        File[] folderEntire = f.listFiles();
        for(File file : folderEntire){
            if(file.isDirectory()){

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        findFile(file);
                    }
                }).start();

            } else{
                writeLog(file);
                //System.out.println("->"+file.getName());
                // System.out.println("->"+file.getAbsolutePath());
            }
        }
    }

    private synchronized void writeLog(File file) {
        String pathLog = "src/main/java/com/manivchuk/pack_1/practise_6/files/copied/";

        try {
            copyFileUsingStream(file,new File(pathLog));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static synchronized void copyFileUsingStream(File source, File dest) throws IOException {
        Files.createDirectories(dest.getParentFile().toPath());
        if(!source.canRead()){
            System.out.println("----->>>>>>>>>COPY");
            source.setReadable(true);
        }
        if(source.canRead()) {
            System.out.println("COPY");

            InputStream is = null;
            OutputStream os = null;
            try {
                is = new FileInputStream(source);
                os = new FileOutputStream(dest);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
            } finally {
                if (is != null) {
                    is.close();
                }
                if (os != null) {
                    os.close();
                }
            }
        }
    }

}
