package com.manivchuk.pack_1.practise_7;
import com.manivchuk.pack_1.practise_7.beans.User;
import com.manivchuk.pack_1.practise_7.beans.UserListDaoUser;

import java.util.*;

public class Start {
    public static void main(String[] args) {
        UserListDaoUser usr = new UserListDaoUser();
        List<User> allUser = usr.getUserList();
        allUser.forEach(System.out::println);

        System.out.println("================------id = 2-----------------==================");

        User userById = usr.getUserById(2);
        System.out.println(userById);

//        System.out.println("==============-------add new user-------------==================");
//        User newUser = new User("hha", "0000","Hhh","Hhh123", "18");
//        usr.updateUser(newUser);
//
//        System.out.println("=====================Show all after add------------=============");
//        UserListDaoUser usr1 = new UserListDaoUser();
//        List<User> allUsers = usr1.getUserList();
//        allUsers.forEach(System.out::println);

        System.out.println("==============-------delete user-------------==================");
        User oldUser = new User("hha", "0000","Hhh","Hhh123", "18");
        usr.deleteUser(oldUser);

        List<User> allU = usr.getUserList();
        allU.forEach(System.out::println);
    }
}
