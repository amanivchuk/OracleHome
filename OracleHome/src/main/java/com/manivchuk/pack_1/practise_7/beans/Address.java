package com.manivchuk.pack_1.practise_7.beans;

public class Address {
    private long id;
    private String city;
    private String street;
    private String numberHome;
    private String numberFlat;

    public Address() {
    }

    public Address(long id, String city, String street, String numberHome, String numberFlat) {
        this.id = id;
        this.city = city;
        this.street = street;
        this.numberHome = numberHome;
        this.numberFlat = numberFlat;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumberHome() {
        return numberHome;
    }

    public void setNumberHome(String numberHome) {
        this.numberHome = numberHome;
    }

    public String getNumberFlat() {
        return numberFlat;
    }

    public void setNumberFlat(String numberFlat) {
        this.numberFlat = numberFlat;
    }
}
