package com.manivchuk.pack_1.practise_7.beans;

import com.manivchuk.pack_1.practise_7.dao.DaoAddress;
import com.manivchuk.pack_1.practise_7.db.Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AddressListDao implements DaoAddress {
    private ArrayList<Address> alltAddress = new ArrayList<>();
    @Override
    public Address createAddress(Address address) {
        return null;
    }

    @Override
    public List<Address> getAllAddress() {
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;

        try {
            conn = Database.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from address");

            while (rs.next()) {
                Address address = new Address();
                address.setId(rs.getLong("id"));
                address.setCity(rs.getString("city"));
                address.setStreet(rs.getString("street"));
                address.setNumberHome(rs.getString("home"));
                address.setNumberFlat(rs.getString("flat"));
                alltAddress.add(address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return alltAddress;
    }

    @Override
    public Address getAddressById(long id) {
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;

        Address address = new Address();

        try {
            conn = Database.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select id, city, street, home, flat from address where id ="+ id);

            while (rs.next()){
                address.setId(rs.getLong("id"));
                address.setCity(rs.getString("city"));
                address.setStreet(rs.getString("street"));
                address.setNumberHome(rs.getString("home"));
                address.setNumberFlat(rs.getString("flat"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (stmt!=null) stmt.close();
                if (rs!=null)rs.close();
                if (conn!=null)conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return address;
    }

    @Override
    public boolean updateAddress(Address address) {
        String sql = "INSERT INTO dao.address (city, street, home, flat) VALUES('" + address.getCity() + "', '" + address.getStreet() + "', '" + address.getNumberHome() + "', '" + address.getNumberFlat() + "');";

        Connection conn = null;
        try {
            conn = Database.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Statement statement = null;
        try{
            statement = conn.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public boolean deleteAddress(Address address) {
        String sql = "delete from dao.address where street = '"+ address.getStreet()+"'";
       Connection conn = null;

        try {
            conn = Database.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Statement stmt = null;
        try {
            stmt =conn.createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if(stmt != null){
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    public List<Address> getAddressList(){
        if(!alltAddress.isEmpty()){
            return alltAddress;
        }else
            return getAllAddress();
    }
}
