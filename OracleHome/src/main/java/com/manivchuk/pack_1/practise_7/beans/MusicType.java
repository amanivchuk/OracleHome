package com.manivchuk.pack_1.practise_7.beans;

public class MusicType {
    private long id;
    private String type;

    public MusicType() {
    }

    public MusicType(long id, String type) {
        this.id = id;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
