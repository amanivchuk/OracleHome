package com.manivchuk.pack_1.practise_7.beans;

import com.manivchuk.pack_1.practise_7.dao.DaoMusicType;
import com.manivchuk.pack_1.practise_7.db.Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MusicTypeListDao implements DaoMusicType {
    private ArrayList<MusicType> allMusicType = new ArrayList<>();

    @Override
    public MusicType createMusicType(User user) {
        return null;
    }

    @Override
    public List<MusicType> getAllMusicType() {
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;

        try {
            conn = Database.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from music_type order by type");

            while (rs.next()) {
                MusicType musicType = new MusicType();
                musicType.setId(rs.getLong("id"));
                musicType.setType(rs.getString("type"));
                allMusicType.add(musicType);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return allMusicType;
    }

    @Override
    public MusicType getMusicTypeById(long id) {
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        MusicType musicType = new MusicType();

        try {
            conn = Database.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select id from music_type where id ="+ id);

            while (rs.next()){
                musicType.setId(rs.getLong("id"));
                musicType.setType(rs.getString("type"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (stmt!=null) stmt.close();
                if (rs!=null)rs.close();
                if (conn!=null)conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return musicType;
    }

    @Override
    public boolean updateMusicType(MusicType musicType) {
        String sql = "INSERT INTO dao.music_type (type) VALUES('" + musicType.getType() + "');";

        Connection conn = null;
        try {
            conn = Database.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Statement statement = null;
        try{
            statement = conn.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public boolean deleteMusicType(MusicType musicType) {
        String sql = "delete from dao.music_type where type = '"+ musicType.getType()+"'";
        Connection conn = null;

        try {
            conn = Database.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Statement stmt = null;
        try {
            stmt =conn.createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if(stmt != null){
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }
    public List<MusicType> getMusicTypeList(){
        if(!allMusicType.isEmpty()){
            return allMusicType;
        }else
            return getAllMusicType();
    }
}
