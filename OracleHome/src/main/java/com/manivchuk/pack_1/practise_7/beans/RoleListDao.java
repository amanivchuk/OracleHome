package com.manivchuk.pack_1.practise_7.beans;

import com.manivchuk.pack_1.practise_7.dao.DaoRole;
import com.manivchuk.pack_1.practise_7.db.Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class RoleListDao implements DaoRole {

    private ArrayList<Role> allRole = new ArrayList<>();

    @Override
    public Role createUser(Role role) {
        return null;
    }

    @Override
    public List<Role> getAllRole() {
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;

        try {
            conn = Database.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from user_role order by role");

            while (rs.next()) {
                Role role = new Role();
                role.setId(rs.getLong("id"));
                role.setRole(rs.getString("role"));
                allRole.add(role);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return allRole;
    }

    @Override
    public Role getRoleById(long id) {
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        Role role = new Role();

        try {
            conn = Database.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select id from user_role where id ="+ id);

            while (rs.next()){
                role.setId(rs.getLong("id"));
                role.setRole(rs.getString("role"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (stmt!=null) stmt.close();
                if (rs!=null)rs.close();
                if (conn!=null)conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return role;
    }

    @Override
    public boolean updateRole(Role role) {
        String sql = "INSERT INTO dao.user_role (role) VALUES('" + role.getRole() + "');";

        Connection conn = null;
        try {
            conn = Database.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Statement statement = null;
        try{
            statement = conn.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public boolean deleteRole(Role role) {
        String sql = "delete from dao.user_role where role = '"+ role.getRole()+"'";
        Connection conn = null;

        try {
            conn = Database.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Statement stmt = null;
        try {
            stmt =conn.createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if(stmt != null){
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    public List<Role> getRoleList(){
        if(!allRole.isEmpty()){
            return allRole;
        }else
            return getAllRole();
    }
}
