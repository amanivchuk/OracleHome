package com.manivchuk.pack_1.practise_7.beans;

public class User {

    private long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String age;

    public User() {
    }

    public User(String login, String password, String firstName, String lastName, String age) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public User(long id, String login, String password, String firstName, String lastName, String age) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", login=" + login +
                ", password=" + password +
                ", firstName=" + firstName +
                ", lastName=" + lastName  +
                ", age=" + age;
    }
}
