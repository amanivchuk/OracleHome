package com.manivchuk.pack_1.practise_7.beans;

import com.manivchuk.pack_1.practise_7.dao.DaoUser;
import com.manivchuk.pack_1.practise_7.db.Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserListDaoUser implements DaoUser {
    private List<User> userList = new ArrayList<>();

    @Override
    public User createUser(User user) {
        return null;
    }

    @Override
    public List<User> getAllUsers() {
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;

         try {
             conn = Database.getConnection();
             stmt = conn.createStatement();
             rs = stmt.executeQuery("select * from users order by firstName");

             while (rs.next()){
                 User user = new User();
                 user.setId(rs.getLong("id"));
                 user.setLogin(rs.getString("login"));
                 user.setPassword(rs.getString("password"));
                 user.setFirstName(rs.getString("firstName"));
                 user.setLastName(rs.getString("lastName"));
                 user.setAge(rs.getString("age"));
                 userList.add(user);
             }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
             try {
                 if (stmt!=null) stmt.close();
                 if (rs!=null)rs.close();
                 if (conn!=null)conn.close();
             } catch (SQLException e) {
                 e.printStackTrace();
             }
         }

        return userList;
    }

    @Override
    public User getUserById(long id) {
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        User user = new User();

        try {
            conn = Database.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select id, login, password, firstName, lastName, age from users where id ="+ id);

            while (rs.next()){
                user.setId(rs.getLong("id"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setFirstName(rs.getString("firstName"));
                user.setLastName(rs.getString("lastName"));
                user.setAge(rs.getString("age"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (stmt!=null) stmt.close();
                if (rs!=null)rs.close();
                if (conn!=null)conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return user;
    }

    @Override
    public boolean updateUser(User user) {

        String sql = "INSERT INTO dao.users (login, password, firstName, lastName, age) VALUES('" + user.getLogin() + "', '" + user.getPassword() + "', '" + user.getFirstName() + "', '" + user.getLastName() + "', '" + user.getAge() + "');";

        Connection conn = null;
        try {
            conn = Database.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Statement statement = null;
        try{
            //conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            //conn.setAutoCommit(false);
            statement = conn.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public boolean deleteUser(User user) {
        String sql = "delete from dao.users where login = '"+ user.getLogin()+"'";
        System.out.println("sql => " + sql);
        Connection conn = null;

        try {
            conn = Database.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Statement stmt = null;
        try {
            stmt =conn.createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if(stmt != null){
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    public List<User> getUserList(){
        if(!userList.isEmpty()){
            return userList;
        }else
            return getAllUsers();
    }
}
