package com.manivchuk.pack_1.practise_7.dao;

import com.manivchuk.pack_1.practise_7.beans.Address;

import java.util.List;

public interface DaoAddress {
    public Address createAddress(Address address);
    public List<Address> getAllAddress();
    public Address getAddressById(long id);
    public boolean updateAddress(Address address);
    public boolean deleteAddress(Address address);
}
