package com.manivchuk.pack_1.practise_7.dao;

import com.manivchuk.pack_1.practise_7.beans.MusicType;
import com.manivchuk.pack_1.practise_7.beans.User;

import java.util.List;

public interface DaoMusicType {
    public MusicType createMusicType(User user);
    public List<MusicType> getAllMusicType();
    public MusicType getMusicTypeById(long id);
    public boolean updateMusicType(MusicType user);
    public boolean deleteMusicType(MusicType user);

}
