package com.manivchuk.pack_1.practise_7.dao;

import com.manivchuk.pack_1.practise_7.beans.Role;

import java.util.List;

public interface DaoRole {
    public Role createUser(Role role);
    public List<Role> getAllRole();
    public Role getRoleById(long id);
    public boolean updateRole(Role role);
    public boolean deleteRole(Role role);

}
