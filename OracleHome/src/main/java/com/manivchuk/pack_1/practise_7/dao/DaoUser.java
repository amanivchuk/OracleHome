package com.manivchuk.pack_1.practise_7.dao;

import com.manivchuk.pack_1.practise_7.beans.User;

import java.util.List;

public interface DaoUser {
    public User createUser(User user);
    public List<User> getAllUsers();
    public User getUserById(long id);
    public boolean updateUser(User user);
    public boolean deleteUser(User user);

}
