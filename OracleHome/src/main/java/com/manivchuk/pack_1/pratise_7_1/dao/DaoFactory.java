package com.manivchuk.pack_1.pratise_7_1.dao;

import com.manivchuk.pack_1.pratise_7_1.dao.api.Dao;
import com.manivchuk.pack_1.pratise_7_1.dao.impl.MovieDaoImpl;
import com.manivchuk.pack_1.pratise_7_1.helper.PropertyHolder;
import com.manivchuk.pack_1.pratise_7_1.model.Movie;

public class DaoFactory {

    private static DaoFactory instance = null;

    private Dao<Integer, Movie> movieDao;

    private DaoFactory(){ loadDaos();}

    public static DaoFactory getInstance(){
        if(instance == null)
            instance = new DaoFactory();
        return instance;
    }

    private void loadDaos() {
        if(PropertyHolder.getInstance().isInMemoryDB()){

        }else {
            movieDao = new MovieDaoImpl(Movie.class);
        }
    }

    public Dao<Integer, Movie> getMovieDao() {
        return movieDao;
    }

    public void setMovieDao(Dao<Integer, Movie> movieDao) {
        this.movieDao = movieDao;
    }
}
