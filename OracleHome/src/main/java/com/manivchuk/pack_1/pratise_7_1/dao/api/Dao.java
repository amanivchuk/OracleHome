package com.manivchuk.pack_1.pratise_7_1.dao.api;

import com.manivchuk.pack_1.pratise_7_1.model.Entity;

import java.util.*;

public interface Dao<K, T extends Entity<K>> {
    List<T> getAll();

    T getById(K key);

    void save(T entity);

    void delete(K key);

    void update(T entity);
}
