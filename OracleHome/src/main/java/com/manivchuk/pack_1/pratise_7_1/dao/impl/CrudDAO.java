package com.manivchuk.pack_1.pratise_7_1.dao.impl;

import com.manivchuk.pack_1.pratise_7_1.dao.api.Dao;
import com.manivchuk.pack_1.pratise_7_1.datasource.DataSource;
import com.manivchuk.pack_1.pratise_7_1.model.Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public abstract class CrudDAO<T extends Entity<Integer>> implements Dao<Integer, T> {

    private Class<T> type;
    private DataSource dataSource;

    private static final String SELECT_ALL = "Select * from %s";
    private static final String FIND_BY_ID = "select * from %s where id = ?";
    private static final String DELETE_BY_ID = "delete from %s where id = ?";

    public CrudDAO(Class<T> type) {
        this.type = type;
        this.dataSource = DataSource.getInstance();
    }

    @Override
    public List<T> getAll() {
        String sql = String.format(SELECT_ALL, type.getSimpleName());
        List result = null;
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery()) {

            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public T getById(Integer key) {
        String sql = String.format(FIND_BY_ID, type.getSimpleName());
        List result = null;
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();) {

            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (T) result.get(0);
    }

    @Override
    public void save(T entity) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = createInsertStatement(connection, entity)) {
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if(resultSet.next()){
                entity.setId((int) resultSet.getLong(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer key) {
        String sql = String.format(DELETE_BY_ID, type.getSimpleName());

        try (Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql)){

            preparedStatement.setInt(1,key);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(T entity) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = createUpdateStatement(connection, entity)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected abstract List readAll(ResultSet resultSet) throws SQLException;
    protected abstract PreparedStatement createInsertStatement(Connection connection, T entity) throws SQLException;
    protected abstract PreparedStatement createUpdateStatement(Connection connection, T entity) throws SQLException;

}
