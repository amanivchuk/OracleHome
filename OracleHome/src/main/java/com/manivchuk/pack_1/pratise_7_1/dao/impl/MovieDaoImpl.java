package com.manivchuk.pack_1.pratise_7_1.dao.impl;

import com.manivchuk.pack_1.pratise_7_1.model.Movie;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class MovieDaoImpl extends CrudDAO<Movie>{

    private final String INSERT = "insert into movie (title, duration, description) values (?,?,?)";
    private final String UPDATE = "update movie set title = ?, duration = ?, description = ?, where id = ?";
    private static MovieDaoImpl crudDAO;

    public MovieDaoImpl(Class type) {
        super(type);
    }

    public static synchronized MovieDaoImpl getInstance(){
        if(crudDAO == null){
            crudDAO = new MovieDaoImpl(Movie.class);
        }
        return crudDAO;
    }

    @Override
    protected List readAll(ResultSet resultSet) throws SQLException {
        List<Movie> result = new LinkedList<>();
        Movie movie = null;
        while (resultSet.next()){
            movie = new Movie();
            movie.setId(resultSet.getInt("id"));
            movie.setTitle(resultSet.getString("title"));
            movie.setDuration(resultSet.getLong("duration"));
            movie.setDescription(resultSet.getString("description"));
            result.add(movie);
        }
        return result;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Movie entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT);
        preparedStatement.setString(1, entity.getTitle());
        preparedStatement.setLong(2, entity.getDuration());
        preparedStatement.setString(3, entity.getDescription());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Movie entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, entity.getTitle());
        preparedStatement.setLong(2, entity.getDuration());
        preparedStatement.setString(3, entity.getDescription());
        preparedStatement.setInt(4,entity.getId());
        return preparedStatement;
    }
}
