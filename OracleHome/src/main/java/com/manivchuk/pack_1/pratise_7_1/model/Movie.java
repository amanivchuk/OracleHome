package com.manivchuk.pack_1.pratise_7_1.model;

public class Movie extends Entity<Integer> {
    private String title;
    private String description;
    private long duration;

    public Movie() {
    }

    public Movie(String title, String description, long duration) {
        this.title = title;
        this.description = description;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie)) return false;
        if(!super.equals(o)) return false;

        Movie movie = (Movie) o;

        if (duration != movie.duration) return false;
        if (title != null ? !title.equals(movie.title) : movie.title != null) return false;
        return description != null ? description.equals(movie.description) : movie.description == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (int) (duration ^ (duration >>> 32));
        return result;
    }
}
