package com.manivchuk.pack_1.pratise_7_1.service.api;

import java.util.*;

public interface Service<K,T> {
    List<T> getAll();

    T getById(K id);

    void save(T entity);

    void delete(K key);

    void update(T entity);
}
