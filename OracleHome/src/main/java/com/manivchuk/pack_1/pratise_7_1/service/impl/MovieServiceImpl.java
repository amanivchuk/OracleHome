package com.manivchuk.pack_1.pratise_7_1.service.impl;

import com.manivchuk.pack_1.home_4.task_4_2.part_3.ListIterable;
import com.manivchuk.pack_1.pratise_7_1.dao.DaoFactory;
import com.manivchuk.pack_1.pratise_7_1.dao.api.Dao;
import com.manivchuk.pack_1.pratise_7_1.dto.MovieDTO;
import com.manivchuk.pack_1.pratise_7_1.mapper.BeanMapper;
import com.manivchuk.pack_1.pratise_7_1.model.Movie;
import com.manivchuk.pack_1.pratise_7_1.service.api.Service;

import java.util.List;

public class MovieServiceImpl implements Service<Integer,MovieDTO>{

    private static MovieServiceImpl service;
    private Dao<Integer,Movie> movieDao;
    private BeanMapper beanMapper;

    private MovieServiceImpl() {
        movieDao = DaoFactory.getInstance().getMovieDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized MovieServiceImpl getInstance(){
        if(service == null)
            service = new MovieServiceImpl();
        return service;
    }

    @Override
    public List<MovieDTO> getAll() {
        List<Movie> movies = movieDao.getAll();
        List<MovieDTO> movieDTOs = beanMapper.listMapToList(movies,MovieDTO.class);
        return movieDTOs;
    }

    @Override
    public MovieDTO getById(Integer id) {
        Movie movie = movieDao.getById(id);
        MovieDTO movieDTO = beanMapper.singleMapper(movie,MovieDTO.class);
        return movieDTO;
    }

    @Override
    public void save(MovieDTO entity) {
        Movie movie = beanMapper.singleMapper(movieDao, Movie.class);
        movieDao.save(movie);
    }

    @Override
    public void delete(Integer key) {

    }

    @Override
    public void update(MovieDTO entity) {

    }
}
