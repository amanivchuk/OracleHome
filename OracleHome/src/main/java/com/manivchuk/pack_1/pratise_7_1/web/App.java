package com.manivchuk.pack_1.pratise_7_1.web;

import com.manivchuk.pack_1.pratise_7_1.dto.MovieDTO;
import com.manivchuk.pack_1.pratise_7_1.service.impl.MovieServiceImpl;

/**
 * Created by ASUS on 14.12.2016.
 */
public class App {
    public static void main(String[] args) {
        MovieDTO movieDTO = new MovieDTO("Title","Description", 25);
        MovieServiceImpl.getInstance().save(movieDTO);
        System.out.println(MovieServiceImpl.getInstance().getById(1));
    }
}
