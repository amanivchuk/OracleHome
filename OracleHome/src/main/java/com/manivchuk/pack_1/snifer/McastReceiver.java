package com.manivchuk.pack_1.snifer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class McastReceiver implements Runnable {
    private int mcastPort = 0;
    private InetAddress mcastAddr = null;
    private InetAddress localHost = null;

    public McastReceiver(int mcastPort, InetAddress mcastAddr) {
        this.mcastPort = mcastPort;
        this.mcastAddr = mcastAddr;
        try{
            localHost = InetAddress.getLocalHost();
            System.out.println("localHost => " + localHost);
            System.out.println("mcastAddr = > "+ mcastAddr);
        } catch (UnknownHostException e) {
            System.out.println("=> Problems identifying localhost.");
            e.printStackTrace(); System.exit(1);
        }
    }

    @Override
    public void run() {
        MulticastSocket mSocket = null;
        try{
            System.out.println("Setting up multicast receiver");
            mSocket = new MulticastSocket(mcastPort);
            mSocket.joinGroup(mcastAddr);
        } catch (IOException e) {
            System.out.println("=> Trouble opening multicast port");
            e.printStackTrace(); System.exit(1);
        }

        DatagramPacket packet;
        System.out.println("Multicast receiver set up");
        while(true){
            try{
                byte[] buf = new byte[1024];
                packet = new DatagramPacket(buf, buf.length);
                System.out.println("McastReceiver: waiting for packet");
                mSocket.receive(packet);


                ByteArrayInputStream bistream = new ByteArrayInputStream(packet.getData());
                ObjectInputStream ois = new ObjectInputStream(bistream);
                Integer value = (Integer) ois.readObject();
                System.out.println("====> Values = " + value);

                System.out.println("InetAddr => "+packet.getAddress());
                System.out.println("localHost => "+localHost);
//                System.out.println("LocalAddr ==>"+packet.getSocketAddress());
//                System.out.println("Port ====>"+packet.getPort());


                //Ignore packets from myself, print the rest
                if(!(packet.getAddress().equals(localHost))){
                    System.out.println("Received multicast packet: " + value.intValue() + " from: " + packet.getAddress());
                }
                ois.close();
                bistream.close();
            }catch (ClassNotFoundException e) {
                System.out.println("=> Class missing while reading mcast packet");
                e.printStackTrace();
            }catch (IOException e) {
                System.out.println("=> Trouble reading multicast message");
                e.printStackTrace(); System.exit(1);
            }
        }
    }
}
