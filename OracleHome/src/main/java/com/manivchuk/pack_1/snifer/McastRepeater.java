package com.manivchuk.pack_1.snifer;
/*
* Send a datagram message every 1 second to a multicast address:port.  The
* datagram contains only and Integer sequence number.
* */


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;

public class McastRepeater  implements Runnable{
    private DatagramSocket datagramSocket = null;
    int mcastPort = 0;
    InetAddress mcastAddr = null;
    InetAddress localHost = null;

    public McastRepeater(int mcastPort, InetAddress mcastAddr) {
        this.mcastPort = mcastPort;
        this.mcastAddr = mcastAddr;
        try{
            datagramSocket = new DatagramSocket();
        } catch (SocketException e) {
            System.out.println("=> Problems creating the datagram socket.");
            e.printStackTrace(); System.exit(1);
        }
        try{
            localHost = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            System.out.println("=> Problems identifying localhost.");
            e.printStackTrace(); System.exit(1);
        }
    }

    @Override
    public void run() {
        DatagramPacket packet = null;
        int count = 0;

        //send multicast msg once per second
        while(true){
            //create the packet to send
            try{
                //serialize the multicast message
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bos);
                out.writeObject(count++);
                out.flush();
                out.close();

                //Create a datagram packet and send it
                packet = new DatagramPacket(bos.toByteArray(), bos.size(), mcastAddr, mcastPort);

                //send the packet
                datagramSocket.send(packet);
                System.out.println("Sending multicast message.");
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                System.out.println("=> error sending multicast");
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
