package com.manivchuk.pack_1.snifer;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class McastTest {
    public static void main(String[] args) {
        int mcastPort = 6000;
        InetAddress mcastAddr = null;

        try{
            mcastAddr = InetAddress.getByName("230.0.0.1");
        } catch (UnknownHostException e) {
            System.out.println("=> Problems getting the symbolic multicast address");
            e.printStackTrace(); System.exit(1);
        }

        //start new thread to receive multicasts
        new Thread(new McastReceiver(mcastPort, mcastAddr), "McastReceiver").start();
        //start new threaf to send multicast
        new Thread(new McastRepeater(mcastPort, mcastAddr), "McastRepeater").start();
    }
}
