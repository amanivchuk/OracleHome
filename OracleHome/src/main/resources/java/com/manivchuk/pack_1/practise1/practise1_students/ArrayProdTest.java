package com.manivchuk.pack_1.practise1.practise1_students;

import com.manivchuk.pack_1.practise1.practise1_arrays.ArrayProd;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
public class ArrayProdTest {
    @Test
    public void prodTest(){
        ArrayProd arr = new ArrayProd(new int[]{2,2,2});
        int res = arr.prod();
        assertEquals("Not equals!",8,res);
    }

    @Test(expected = NullPointerException.class)
    public void nullPointerTest(){
        new ArrayProd(new int[]{}).prod();
    }
}
