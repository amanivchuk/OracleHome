package com.manivchuk.pack_1.practise1.practise1_students;

import com.manivchuk.pack_1.practise1.practise1_arrays.ArraySum;
import com.manivchuk.pack_1.practise1.practise1_arrays.ArraySumExt;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ArraySumTest {
    @Test
    public void arraySumTestEquals(){
        int result = ArraySum.sum(new int[]{1, 2, 3});
        assertEquals("6 = 6",6,result);
    }
    @Test
    public void arraySumTestNotEquals(){
        int result = ArraySum.sum(new int[]{1,2,3});
        assertNotEquals("5 != 6", 5, result);
    }

/////////////////////////////////////////////

    private ArraySumExt arr = null;
    @Before
    public void setArray(){
        arr = new ArraySumExt(new int[]{3,3,3});
    }

    @Test
    public void sumTest(){
        int answer = ArraySumExt.sum();
        assertEquals("9", 9, answer);
    }

    @After
    public void tearArray(){
        arr = null;
    }
////////////////////////////////////////////////////
    @Test(expected = NullPointerException.class)
    public void nullPointerTest(){
        new ArraySumExt(null);
        ArraySumExt.sum();
    }

}
