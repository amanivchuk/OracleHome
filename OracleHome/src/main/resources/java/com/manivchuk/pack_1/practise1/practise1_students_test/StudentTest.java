package com.manivchuk.pack_1.practise1.practise1_students_test;

import com.manivchuk.pack_1.practise1.practise1_students.Exam;
import com.manivchuk.pack_1.practise1.practise1_students.ExamException;
import com.manivchuk.pack_1.practise1.practise1_students.Student;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

public class StudentTest {
    static Student student = null;
    @BeforeClass
    public static void createUser(){
        student = new Student("Ivan", "Ivanov");
        Exam math1 = new Exam("Mathematic",3,"20.10.2016",2);
        Exam math2 = new Exam("Mathematic",5,"20.10.2016",2);
        Exam math3 = new Exam("Mathematic",4,"20.10.2016",2);
        Exam chemist = new Exam("Chemist",5,"14.10.2016",2);
        Exam liter = new Exam("Literature",4,"12.09.2016",2);
        student.addRatingForExam(math1);
        student.addRatingForExam(math2);
        student.addRatingForExam(math3);
        student.addRatingForExam(liter);
        student.addRatingForExam(chemist);
    }

    @Test
    public void deleteRatingForUserTest(){
        for(Exam exam : student.getAllExam()){
            exam.setRating(1);
        }
        for(Exam exam : student.getAllExam()){
            System.out.println("Rating = " + exam.getRating());
        }
    }

    @Test
    public void highestRatingUserTest(){
        int rating = 0;
        String subject = "Mathematic";
        for(Exam exam : student.getAllExam()){
            if(subject.equalsIgnoreCase(exam.getSubjectName())){
                if(rating < exam.getRating()){
                    rating = exam.getRating();
                }
            }
        }
        System.out.println(" Highest rating =" + rating);
    }

    @Test
    public void ExcepExamIfNotTest() throws ExamException {
        Exam phisics = new Exam("Phisics",5,"21.11.2015",1);
        for(Exam exam : student.getAllExam()){
            if(!exam.getSubjectName().equalsIgnoreCase(phisics.getSubjectName()))
                throw new ExamException();
        }
    }

    @Test
    public void CountExamWithRatingTest(){
        int countRating = 5;
        int countOfExam = 0;
        for(Exam exam : student.getAllExam()){
            if(countRating == exam.getRating()){
                countOfExam++;
            }
        }
        System.out.println("Count exam with rating 5 is: " + countOfExam);
    }

    @Test
    public void MiddleRatingOfSemestrTest(){
        int raiting = 0;
        int countExam = 0;
        int semestr = 2;
        for(Exam exam : student.getAllExam()){
            if(semestr == exam.getSemestr()){
                raiting += exam.getRating();
                countExam++;
            }
        }
        System.out.println("Middle raiting for second semestr = " + raiting/countExam);
    }
    @After
    public void AfterTest(){}
}
