package com.manivchuk.pack_1.home_2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringUtilTest {
    @Test
    public void reverseStr(){
        String res = StringUtils.reverseString("Hello");
        assertEquals("result = olleH","olleH",res);
    }
    @Test
    public void isPalindromeTest(){
        assertEquals("true",true,StringUtils.isPalindrome("А роза упала на лапу Азора"));
    }
    @Test
    public void moreTen(){
        String str1 = "aassddffggh";
        assertEquals("aassdd", "aassdd", StringUtils.notMoreTen(str1));

        String str2 = "aassddffg";
        assertEquals("aassddffgooo", "aassddffgooo", StringUtils.notMoreTen(str2));
    }
    @Test
    public void changeFirstAndLastWordTest(){
        String str = "first second third last";
        assertEquals("last second third first", "last second third first ", StringUtils.changeFirstAndLastWord(str));
    }
    @Test
    public void hasABCTest(){
        String str1 = "ddd fff ggg";
        assertEquals("false", false, StringUtils.hasABC(str1));
        String str2 = "aaa bbbb f";
        assertEquals("true", true, StringUtils.hasABC(str2));
    }
    @Test
    public void strFormatDateTest(){
        String date = "22.10.2016";
        assertEquals("true",true,StringUtils.strFormatDate(date));
    }
    @Test
    public void strPostAddressTest(){
        String mail = "amanivchuk@gmail.com";
        assertEquals("true",true,StringUtils.strPostAddress(mail));
    }
    @Test
    public void findCorrectTelTest(){
        String phone1 = "+3(067)333-33-33; +3(066)444-44-44; +3(097)555-55-55; 33-33-33";
        String phone2 = "+3(067)333-33-33; +3(066)444-44-44; +3(097)555-55-55; ";
        //System.out.println(phone1);
        assertEquals("Equal", phone2, StringUtils.findCorrectTel(phone1));

    }
    @Test
    public void zamenaSlovPredTest(){
        String str = "Aaaa bbb. Sssss ggg eee.";
        String res = "bbb Aaaa. eee ggg Sssss. ";
        assertEquals("test", res,StringUtils.ZamenaPred(str));
    }
}
