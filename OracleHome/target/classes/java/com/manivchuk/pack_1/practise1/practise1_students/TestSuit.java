package com.manivchuk.pack_1.practise1.practise1_students;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by ASUS on 01.11.2016.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ArraySumTest.class, ArrayProdTest.class})
public class TestSuit {

}
